module type RouterConfig = {
  type t;
  let make: ReasonReact.Router.url => t;
  let toString: t => string;
};

module Make = (Config: RouterConfig) => {
  let navigate = (route: Config.t) =>
    ReasonReact.Router.push(route->Config.toString);

  let replace = (route: Config.t) =>
    ReasonReact.Router.replace(route->Config.toString);

  let routerContext =
    React.createContext(
      ReasonReact.Router.dangerouslyGetInitialUrl()->Config.make,
    );

  module RouterContextProvider = {
    let makeProps = (~value: Config.t, ~children, ()) => {
      "value": value,
      "children": children,
    };

    let make = React.Context.provider(routerContext);
  };

  module Provider = {
    [@react.component]
    let make = (~children) => {
      let (currentRoute, setCurrentRoute) =
        React.useState(() =>
          ReasonReact.Router.dangerouslyGetInitialUrl()->Config.make
        );

      React.useLayoutEffect1(
        () => {
          let watcherID =
            ReasonReact.Router.watchUrl(url =>
              setCurrentRoute(_ => url |> Config.make)
            );
          Some(() => ReasonReact.Router.unwatchUrl(watcherID));
        },
        [|setCurrentRoute|],
      );

      <RouterContextProvider value=currentRoute>
        {children(~currentRoute)}
      </RouterContextProvider>;
    };
  };

  let isRouteEqual = (routeA: Config.t, routeB: Config.t): bool => {
    routeA->Config.toString === routeB->Config.toString;
  };

  let useIsCurrentRoute = (route: Config.t): bool => {
    let currentRoute = React.useContext(routerContext);
    isRouteEqual(currentRoute, route);
  };

  module Link = {
    [@react.component]
    let make =
        (
          ~route: Config.t,
          ~className="",
          ~activeClassName="",
          ~onClick: option(ReactEvent.Mouse.t => unit)=?,
          ~children,
        ) => {
      let isCurrentRoute = useIsCurrentRoute(route);
      let activeClassName = isCurrentRoute ? activeClassName : "";

      <a
        href={route->Config.toString}
        className={j|$className $activeClassName|j}
        onClick={event =>
          if (!event->ReactEvent.Mouse.defaultPrevented
              && event->ReactEvent.Mouse.button == 0
              && !event->ReactEvent.Mouse.altKey
              && !event->ReactEvent.Mouse.ctrlKey
              && !event->ReactEvent.Mouse.metaKey
              && !event->ReactEvent.Mouse.shiftKey) {
            event->ReactEvent.Mouse.preventDefault;
            onClick->Option.forEach(fn => fn(event));
            route->navigate;
          }
        }>
        children
      </a>;
    };
  };

  module Redirect = {
    type behavior =
      | Push
      | Replace;

    [@react.component]
    let make = (~to_: Config.t, ~behavior=Replace) => {
      React.useEffect0(() => {
        switch (behavior) {
        | Push => navigate(to_)
        | Replace => replace(to_)
        };
        None;
      });

      React.null;
    };
  };
};
