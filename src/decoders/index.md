# Decoders

## API

### Decode an enumeration

```reasonml
module LiftEnum = {
  [@bs.deriving jsConverter]
  type enum = [
    | `withLift
    | `withoutLift
    | `unknown
  ];
};

module Lift = Toolkit.Decoder.Enum(LiftEnum);

/**
 * Later
 **/
[@decco]
type response = {
  lift: Lift.t
}
```

### Decode a date

```reasonml
[@decco]
type response = {
  createdAt: Toolkit.Decoder.Date.t
}
```

### Decode unit measure

There are 2 options :

- the field has the unit like : `19 mm`
- the field has only the value

#### Automatic unit handling

```reasonml
[@decco]
type response = {
  length: Toolkit.Decoder.UnitMeasure.Dimension.WithUnit.t
}
```

#### Known unit

```reasonml
[@decco]
type response = {
  weight: Toolkit.Decoder.UnitMeasure.Weight.Kg.t
}
```

### Decode an Array written as a String

This codec is intended for encoding the array params of API requests of scala services.

```reasonml
[@decco]
type params = {
  lengths: Toolkit.Decoder.StringArray(Toolkit.Decoder.UnitMeasure.Dimension.WithUnit.t)
};

let params = {lengths:[|`cm(1),`m(40),`km(3)|]};
let encodedParams = params->params_encode; // "1.00 cm,40.00 m,3.00 km"
let decodedParams = encodedParams->params_decode; // Ok([|`cm(1),`m(40),`km(3)|])
```
