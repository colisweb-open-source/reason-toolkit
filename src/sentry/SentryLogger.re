open BsSentry;

let error = (loc: ReScriptLogger.Location.t, event) => {
  Sentry.(
    withScope(scope => {
      scope->Scope.setExtra("module", loc.subModulePath);
      captureMessage(event);
    })
  );
};

let error1 = (loc: ReScriptLogger.Location.t, event, (label, data)) =>
  Sentry.(
    withScope(scope => {
      scope->Scope.setExtra("module", loc.subModulePath);
      scope->Scope.setExtra(label, data);
      captureMessage(event);
    })
  );

let error2 =
    (
      loc: ReScriptLogger.Location.t,
      event,
      (label1, data1),
      (label2, data2),
    ) =>
  Sentry.(
    withScope(scope => {
      scope->Scope.setExtra("module", loc.subModulePath);
      scope->Scope.setExtra(label1, data1);
      scope->Scope.setExtra(label2, data2);
      captureMessage(event);
    })
  );

let errorWithData = (loc: ReScriptLogger.Location.t, event, (label, data)) =>
  Sentry.(
    withScope(scope => {
      scope->Scope.setExtra("module", loc.subModulePath);
      scope->Scope.setExtra(label, data);
      captureMessage(event);
    })
  );

let errorWithData2 =
    (
      loc: ReScriptLogger.Location.t,
      event,
      (label1, data1),
      (label2, data2),
    ) =>
  Sentry.(
    withScope(scope => {
      scope->Scope.setExtra("module", loc.subModulePath);
      scope->Scope.setExtra(label1, data1);
      scope->Scope.setExtra(label2, data2);
      captureMessage(event);
    })
  );
