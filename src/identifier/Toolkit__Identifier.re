module MakeString = (()) => {
  module Id: {
    [@decco]
    type t;
  } = {
    [@decco]
    type t = string;
  };

  [@decco]
  type t = Id.t;
  external make: string => t = "%identity";
  external toString: t => string = "%identity";
};

module MakeInt = (()) => {
  module Id: {
    [@decco]
    type t;
  } = {
    [@decco]
    type t = int;
  };

  [@decco]
  type t = Id.t;
  external make: int => t = "%identity";
  external toInt: t => int = "%identity";
  external magic: 'a => t = "%identity";

  let toString = t => t->toInt->Int.toString;
  let castAsString = t => t->toInt->Int.toString->magic;
};
