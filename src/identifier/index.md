# Identifier

Convert a string or an int in an opaque type with a decco decoder.
Mostly used for identifiers.

## API

### `string` identifier

```reasonml
module DeliveryId = Toolkit.Identifier.MakeString({});

/**
 somewhere
**/
[@decco]
type response = {
  deliveryId: DeliveryId.t
};

deliveryId->DeliveryId.toString
```

### `int` identifier

```reasonml
module DeliveryId = Toolkit.Identifier.MakeInt({});

/**
 somewhere
**/
[@decco]
type response = {
  deliveryId: DeliveryId.t
};

deliveryId->DeliveryId.toString;
deliveryId->DeliveryId.toInt;
```
