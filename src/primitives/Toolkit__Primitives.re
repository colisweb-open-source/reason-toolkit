module String = {
  let joinNonEmty = (~separator=", ", parts: array(string)): string =>
    parts->Array.keep(str => str !== "") |> Js.Array.joinWith(separator);

  /** TODO: remove */
  let join = joinNonEmty;
};

module Option = {
  let fromString = v => v === "" ? None : Some(v);

  let fromBool = v => v === true ? Some(true) : None;

  let toString =
    fun
    | Some(v) => v
    | None => "";

  let join = (~separator=" ", parts) =>
    (parts->Array.keepMap(v => v) |> Js.Array.joinWith(separator))
    ->fromString;

  let mapString = opt => opt->Option.flatMap(fromString);

  let map2 = (opt, cb) => {
    let (opt1, opt2) = opt;

    opt1->Option.flatMap(v1 => opt2->Option.map(v2 => cb(v1, v2)));
  };

  let mapBool = opt => opt->Option.flatMap(fromBool);

  let areEquals =
      (a: option('a), b: option('a), compare: ('a, 'a) => bool): bool =>
    switch (a, b) {
    | (Some(a), Some(b)) => compare(a, b)
    | (None, None) => true
    | _ => false
    };
};

module Array = {
  let joinNonEmpty = (~separator=", ", parts: array(string)): string =>
    parts->Array.keep(str => str !== "") |> Js.Array.joinWith(separator);

  let tail = array => array->Array.get(array->Array.length - 1);
  let tailExn = array => array->Array.getExn(array->Array.length - 1);
  let isLastIndex = (array, index) => array->Array.length - 1 == index;
};

module Result = {
  let get = result =>
    switch (result) {
    | Ok(v) => Some(v)
    | Error(_) => None
    };
};
