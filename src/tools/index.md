# Mock

Use [Mock Service Worker](https://mswjs.io/) and [FakerJs](https://github.com/marak/Faker.js/) to generate fake data and mock endpoints from an [OpenAPI](https://swagger.io/specification/) schema.

## Usage

This will display a monitor in the top right corner of your app where you can enable/disable the endpoints.

```reasonml
[@react.component]
let make = () => {
  let schemas: array(Mock.mockOptions('a)) = [|
    {
      url: "http://localhost:3000",
      schema: [%bs.raw "require('/refs/v5.yaml')"],
      resolver:
        Some(
          (req, _, _, next) => {
            req.headers
              ->Msw.setHeader("X-Header", "My Value");
            next();
          },
        ),
    },
  |];

  let worker = Msw.setupWorker([||]);
  worker->Msw.startWorker;
  let onChange = _ => Js.log("reload");

  <Mock.Monitor schemas onChange worker />
}
```
