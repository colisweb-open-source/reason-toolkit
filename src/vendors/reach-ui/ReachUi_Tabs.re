include ReachUi_Utils;

module Tabs = {
  [@bs.module "@reach/tabs"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~index: option(int)=?,
      ~defaultIndex: option(int)=?,
      ~onChange: option(int => unit)=?,
      ~className: option(string)=?
    ) =>
    React.element =
    "Tabs";
};

module TabsRender = {
  type renderProps = {
    selectedIndex: int,
    focusedIndex: int,
  };

  [@bs.module "@reach/tabs"] [@react.component]
  external make:
    (
      ~children: renderProps => React.element,
      ~index: option(int)=?,
      ~defaultIndex: option(int)=?,
      ~onChange: option(int => unit)=?,
      ~className: option(string)=?
    ) =>
    React.element =
    "Tabs";
};

module TabList = {
  [@bs.module "@reach/tabs"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~_as: option(as_)=?,
      ~className: option(string)=?
    ) =>
    React.element =
    "TabList";
};

module TabPanels = {
  [@bs.module "@reach/tabs"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~_as: option(as_)=?,
      ~className: option(string)=?
    ) =>
    React.element =
    "TabPanels";
};

module TabPanel = {
  [@bs.module "@reach/tabs"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~_as: option(as_)=?,
      ~className: option(string)=?
    ) =>
    React.element =
    "TabPanel";
};

module Tab = {
  [@bs.module "@reach/tabs"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~_as: option(as_)=?,
      ~className: option(string)=?,
      ~disabled: option(bool)=?,
      ~isSelected: option(bool)=?
    ) =>
    React.element =
    "Tab";
};
