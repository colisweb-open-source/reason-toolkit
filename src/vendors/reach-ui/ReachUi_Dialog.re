module Dialog = {
  type props = {
    .
    "isOpen": bool,
    "onDismiss": unit => unit,
    "aria-label": option(string),
    "aria-labelledby": option(string),
    // "allowPinchZoom": option(bool),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/dialog"]
  external make: React.component(props) = "Dialog";

  let makeProps =
      (
        ~isOpen: bool,
        ~onDismiss: unit => unit,
        ~ariaLabel: option(string)=?,
        ~ariaLabelledby: option(string)=?,
        // Disable allowPinchZoom for now since it causes an annoying react dom warning
        // ~allowPinchZoom: option(bool)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "isOpen": isOpen,
    "onDismiss": onDismiss,
    "aria-label": ariaLabel,
    "aria-labelledby": ariaLabelledby,
    "style": style,
    "className": className,
    // "allowPinchZoom": allowPinchZoom,
    "children": children,
  };
};

module DialogContent = {
  type props = {
    .
    "aria-label": option(string),
    "aria-labelledby": option(string),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/dialog"]
  external make: React.component(props) = "DialogContent";

  let makeProps =
      (
        ~ariaLabel: option(string)=?,
        ~ariaLabelledby: option(string)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "aria-label": ariaLabel,
    "aria-labelledby": ariaLabelledby,
    "style": style,
    "className": className,
    "children": children,
  };
};

module DialogOverlay = {
  [@bs.module "@reach/dialog"] [@react.component]
  external make:
    (
      ~initialFocusRef: React.ref('a)=?,
      ~allowPinchZoom: bool=?,
      ~isOpen: bool,
      ~onDismiss: unit => unit,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "DialogOverlay";
};
