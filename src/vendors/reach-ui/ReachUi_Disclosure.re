module Disclosure = {
  [@bs.module "@reach/disclosure"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~_open: bool=?,
      ~defaultOpen: bool=?,
      ~id: string=?,
      ~onChange: unit => unit=?
    ) =>
    React.element =
    "Disclosure";
};

module DisclosureButton = {
  [@bs.module "@reach/disclosure"] [@react.component]
  external make:
    (~children: React.element, ~className: string=?) => React.element =
    "DisclosureButton";
};

module DisclosurePanel = {
  [@bs.module "@reach/disclosure"] [@react.component]
  external make: (~children: React.element) => React.element =
    "DisclosurePanel";
};