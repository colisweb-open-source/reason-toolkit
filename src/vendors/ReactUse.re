[@bs.module "react-use"]
external useDebounce:
  (unit => unit, int, array('a)) => (Js.Nullable.t(bool), unit => unit) =
  "useDebounce";

[@bs.module "react-use"]
external useDebounce2:
  (unit => unit, int, ('a, 'b)) => (Js.Nullable.t(bool), unit => unit) =
  "useDebounce";

[@bs.module "react-use"]
external useDebounce3:
  (unit => unit, int, ('a, 'b, 'c)) => (Js.Nullable.t(bool), unit => unit) =
  "useDebounce";

[@bs.module "react-use"]
external useDebounce4:
  (unit => unit, int, ('a, 'b, 'c, 'd)) =>
  (Js.Nullable.t(bool), unit => unit) =
  "useDebounce";

[@bs.module "react-use"]
external useTimeoutFn:
  (unit => unit, int) => (Js.Nullable.t(bool), unit => unit) =
  "useTimeoutFn";

[@bs.module "react-use"] external usePrevious: 'a => 'a = "usePrevious";

[@bs.module "react-use"]
external useFirstMountState: unit => bool = "useFirstMountState";

[@bs.module "react-use"]
external useMountedState: (. unit) => (. unit) => bool = "useMountedState";

[@bs.module "react-use"]
external useUpdateEffect: (unit => option(unit => unit), array('a)) => unit =
  "useUpdateEffect";

[@bs.module "react-use"]
external useUpdateEffect2: (unit => option(unit => unit), ('a, 'b)) => unit =
  "useUpdateEffect";

[@bs.module "react-use"]
external useUpdateEffect3:
  (unit => option(unit => unit), ('a, 'b, 'c)) => unit =
  "useUpdateEffect";

[@bs.module "react-use"]
external usePreviousDistinct:
  (~compare: option(('a, 'b) => bool)=?, 'a, unit) => 'a =
  "usePreviousDistinct";

[@bs.module "react-use"]
external useGetSet: (unit => 'a) => ('a, 'a => 'a) = "useGetSet";
