module Md3dRotation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "Md3dRotation";
};

module MdAccessibility = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccessibility";
};

module MdAccessible = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccessible";
};

module MdAccountBalance = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccountBalance";
};

module MdAccountBalanceWallet = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccountBalanceWallet";
};

module MdAccountBox = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccountBox";
};

module MdAccountCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccountCircle";
};

module MdAddShoppingCart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddShoppingCart";
};

module MdAlarm = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAlarm";
};

module MdAlarmAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAlarmAdd";
};

module MdAlarmOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAlarmOff";
};

module MdAlarmOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAlarmOn";
};

module MdAllOut = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAllOut";
};

module MdAndroid = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAndroid";
};

module MdAnnouncement = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAnnouncement";
};

module MdAspectRatio = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAspectRatio";
};

module MdAssessment = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssessment";
};

module MdAssignment = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssignment";
};

module MdAssignmentInd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssignmentInd";
};

module MdAssignmentLate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssignmentLate";
};

module MdAssignmentReturn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssignmentReturn";
};

module MdAssignmentReturned = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssignmentReturned";
};

module MdAssignmentTurnedIn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssignmentTurnedIn";
};

module MdAutorenew = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAutorenew";
};

module MdBackup = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBackup";
};

module MdBook = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBook";
};

module MdBookmark = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBookmark";
};

module MdBookmarkBorder = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBookmarkBorder";
};

module MdBugReport = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBugReport";
};

module MdBuild = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBuild";
};

module MdCached = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCached";
};

module MdCameraEnhance = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCameraEnhance";
};

module MdCardGiftcard = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCardGiftcard";
};

module MdCardMembership = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCardMembership";
};

module MdCardTravel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCardTravel";
};

module MdChangeHistory = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChangeHistory";
};

module MdCheckCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCheckCircle";
};

module MdChromeReaderMode = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChromeReaderMode";
};

module MdClass = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdClass";
};

module MdCode = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCode";
};

module MdCompareArrows = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCompareArrows";
};

module MdCopyright = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCopyright";
};

module MdCreditCard = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCreditCard";
};

module MdDashboard = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDashboard";
};

module MdDateRange = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDateRange";
};

module MdDelete = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDelete";
};

module MdDeleteForever = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDeleteForever";
};

module MdDescription = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDescription";
};

module MdDns = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDns";
};

module MdDone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDone";
};

module MdDoneAll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDoneAll";
};

module MdDonutLarge = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDonutLarge";
};

module MdDonutSmall = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDonutSmall";
};

module MdEject = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEject";
};

module MdEuroSymbol = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEuroSymbol";
};

module MdEvent = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEvent";
};

module MdEventSeat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEventSeat";
};

module MdExitToApp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExitToApp";
};

module MdExplore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExplore";
};

module MdExtension = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExtension";
};

module MdFace = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFace";
};

module MdFavorite = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFavorite";
};

module MdFavoriteBorder = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFavoriteBorder";
};

module MdFeedback = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFeedback";
};

module MdFindInPage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFindInPage";
};

module MdFindReplace = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFindReplace";
};

module MdFingerprint = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFingerprint";
};

module MdFlightLand = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlightLand";
};

module MdFlightTakeoff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlightTakeoff";
};

module MdFlipToBack = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlipToBack";
};

module MdFlipToFront = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlipToFront";
};

module MdGTranslate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGTranslate";
};

module MdGavel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGavel";
};

module MdGetApp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGetApp";
};

module MdGif = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGif";
};

module MdGrade = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGrade";
};

module MdGroupWork = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGroupWork";
};

module MdHelp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHelp";
};

module MdHelpOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHelpOutline";
};

module MdHighlightOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHighlightOff";
};

module MdHistory = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHistory";
};

module MdHome = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHome";
};

module MdHourglassEmpty = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHourglassEmpty";
};

module MdHourglassFull = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHourglassFull";
};

module MdHttp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHttp";
};

module MdHttps = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHttps";
};

module MdImportantDevices = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdImportantDevices";
};

module MdInfo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInfo";
};

module MdInfoOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInfoOutline";
};

module MdInput = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInput";
};

module MdInvertColors = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInvertColors";
};

module MdLabel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLabel";
};

module MdLabelOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLabelOutline";
};

module MdLanguage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLanguage";
};

module MdLaunch = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLaunch";
};

module MdLightbulbOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLightbulbOutline";
};

module MdLineStyle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLineStyle";
};

module MdLineWeight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLineWeight";
};

module MdList = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdList";
};

module MdLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLock";
};

module MdLockOpen = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLockOpen";
};

module MdLockOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLockOutline";
};

module MdLoyalty = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLoyalty";
};

module MdMarkunreadMailbox = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMarkunreadMailbox";
};

module MdMotorcycle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMotorcycle";
};

module MdNoteAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNoteAdd";
};

module MdOfflinePin = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdOfflinePin";
};

module MdOpacity = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdOpacity";
};

module MdOpenInBrowser = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdOpenInBrowser";
};

module MdOpenInNew = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdOpenInNew";
};

module MdOpenWith = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdOpenWith";
};

module MdPageview = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPageview";
};

module MdPanTool = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPanTool";
};

module MdPayment = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPayment";
};

module MdPermCameraMic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermCameraMic";
};

module MdPermContactCalendar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermContactCalendar";
};

module MdPermDataSetting = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermDataSetting";
};

module MdPermDeviceInformation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermDeviceInformation";
};

module MdPermIdentity = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermIdentity";
};

module MdPermMedia = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermMedia";
};

module MdPermPhoneMsg = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermPhoneMsg";
};

module MdPermScanWifi = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPermScanWifi";
};

module MdPets = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPets";
};

module MdPictureInPicture = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPictureInPicture";
};

module MdPictureInPictureAlt = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPictureInPictureAlt";
};

module MdPlayForWork = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlayForWork";
};

module MdPolymer = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPolymer";
};

module MdPowerSettingsNew = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPowerSettingsNew";
};

module MdPregnantWoman = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPregnantWoman";
};

module MdPrint = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPrint";
};

module MdQueryBuilder = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdQueryBuilder";
};

module MdQuestionAnswer = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdQuestionAnswer";
};

module MdReceipt = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReceipt";
};

module MdRecordVoiceOver = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRecordVoiceOver";
};

module MdRedeem = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRedeem";
};

module MdRemoveShoppingCart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRemoveShoppingCart";
};

module MdReorder = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReorder";
};

module MdReportProblem = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReportProblem";
};

module MdRestore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRestore";
};

module MdRestorePage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRestorePage";
};

module MdRoom = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRoom";
};

module MdRoundedCorner = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRoundedCorner";
};

module MdRowing = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRowing";
};

module MdSchedule = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSchedule";
};

module MdSearch = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSearch";
};

module MdSettings = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettings";
};

module MdSettingsApplications = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsApplications";
};

module MdSettingsBackupRestore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsBackupRestore";
};

module MdSettingsBluetooth = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsBluetooth";
};

module MdSettingsBrightness = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsBrightness";
};

module MdSettingsCell = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsCell";
};

module MdSettingsEthernet = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsEthernet";
};

module MdSettingsInputAntenna = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsInputAntenna";
};

module MdSettingsInputComponent = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsInputComponent";
};

module MdSettingsInputComposite = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsInputComposite";
};

module MdSettingsInputHdmi = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsInputHdmi";
};

module MdSettingsInputSvideo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsInputSvideo";
};

module MdSettingsOverscan = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsOverscan";
};

module MdSettingsPhone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsPhone";
};

module MdSettingsPower = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsPower";
};

module MdSettingsRemote = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsRemote";
};

module MdSettingsVoice = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsVoice";
};

module MdShop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShop";
};

module MdShopTwo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShopTwo";
};

module MdShoppingBasket = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShoppingBasket";
};

module MdShoppingCart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShoppingCart";
};

module MdSpeakerNotes = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpeakerNotes";
};

module MdSpeakerNotesOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpeakerNotesOff";
};

module MdSpellcheck = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpellcheck";
};

module MdStars = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStars";
};

module MdStore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStore";
};

module MdSubject = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSubject";
};

module MdSupervisorAccount = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSupervisorAccount";
};

module MdSwapHoriz = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSwapHoriz";
};

module MdSwapVert = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSwapVert";
};

module MdSwapVerticalCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSwapVerticalCircle";
};

module MdSystemUpdateAlt = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSystemUpdateAlt";
};

module MdTab = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTab";
};

module MdTabUnselected = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTabUnselected";
};

module MdTheaters = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTheaters";
};

module MdThumbDown = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdThumbDown";
};

module MdThumbUp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdThumbUp";
};

module MdThumbsUpDown = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdThumbsUpDown";
};

module MdTimeline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTimeline";
};

module MdToc = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdToc";
};

module MdToday = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdToday";
};

module MdToll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdToll";
};

module MdTouchApp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTouchApp";
};

module MdTrackChanges = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTrackChanges";
};

module MdTranslate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTranslate";
};

module MdTrendingDown = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTrendingDown";
};

module MdTrendingFlat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTrendingFlat";
};

module MdTrendingUp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTrendingUp";
};

module MdTurnedIn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTurnedIn";
};

module MdTurnedInNot = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTurnedInNot";
};

module MdUpdate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdUpdate";
};

module MdVerifiedUser = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVerifiedUser";
};

module MdViewAgenda = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewAgenda";
};

module MdViewArray = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewArray";
};

module MdViewCarousel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewCarousel";
};

module MdViewColumn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewColumn";
};

module MdViewDay = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewDay";
};

module MdViewHeadline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewHeadline";
};

module MdViewList = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewList";
};

module MdViewModule = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewModule";
};

module MdViewQuilt = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewQuilt";
};

module MdViewStream = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewStream";
};

module MdViewWeek = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewWeek";
};

module MdVisibility = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVisibility";
};

module MdVisibilityOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVisibilityOff";
};

module MdWatchLater = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWatchLater";
};

module MdWork = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWork";
};

module MdYoutubeSearchedFor = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdYoutubeSearchedFor";
};

module MdZoomIn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdZoomIn";
};

module MdZoomOut = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdZoomOut";
};

module MdAddAlert = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddAlert";
};

module MdError = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdError";
};

module MdErrorOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdErrorOutline";
};

module MdWarning = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWarning";
};

module MdAddToQueue = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddToQueue";
};

module MdAirplay = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirplay";
};

module MdAlbum = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAlbum";
};

module MdArtTrack = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArtTrack";
};

module MdAvTimer = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAvTimer";
};

module MdBrandingWatermark = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrandingWatermark";
};

module MdCallToAction = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallToAction";
};

module MdClosedCaption = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdClosedCaption";
};

module MdEqualizer = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEqualizer";
};

module MdExplicit = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExplicit";
};

module MdFastForward = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFastForward";
};

module MdFastRewind = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFastRewind";
};

module MdFeaturedPlayList = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFeaturedPlayList";
};

module MdFeaturedVideo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFeaturedVideo";
};

module MdFiberDvr = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFiberDvr";
};

module MdFiberManualRecord = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFiberManualRecord";
};

module MdFiberNew = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFiberNew";
};

module MdFiberPin = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFiberPin";
};

module MdFiberSmartRecord = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFiberSmartRecord";
};

module MdForward10 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdForward10";
};

module MdForward30 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdForward30";
};

module MdForward5 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdForward5";
};

module MdGames = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGames";
};

module MdHd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHd";
};

module MdHearing = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHearing";
};

module MdHighQuality = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHighQuality";
};

module MdLibraryAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLibraryAdd";
};

module MdLibraryBooks = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLibraryBooks";
};

module MdLibraryMusic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLibraryMusic";
};

module MdLoop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLoop";
};

module MdMic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMic";
};

module MdMicNone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMicNone";
};

module MdMicOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMicOff";
};

module MdMovie = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMovie";
};

module MdMusicVideo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMusicVideo";
};

module MdNewReleases = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNewReleases";
};

module MdNotInterested = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNotInterested";
};

module MdNote = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNote";
};

module MdPause = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPause";
};

module MdPauseCircleFilled = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPauseCircleFilled";
};

module MdPauseCircleOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPauseCircleOutline";
};

module MdPlayArrow = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlayArrow";
};

module MdPlayCircleFilled = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlayCircleFilled";
};

module MdPlayCircleOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlayCircleOutline";
};

module MdPlaylistAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlaylistAdd";
};

module MdPlaylistAddCheck = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlaylistAddCheck";
};

module MdPlaylistPlay = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlaylistPlay";
};

module MdQueue = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdQueue";
};

module MdQueueMusic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdQueueMusic";
};

module MdQueuePlayNext = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdQueuePlayNext";
};

module MdRadio = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRadio";
};

module MdRecentActors = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRecentActors";
};

module MdRemoveFromQueue = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRemoveFromQueue";
};

module MdRepeat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRepeat";
};

module MdRepeatOne = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRepeatOne";
};

module MdReplay10 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReplay10";
};

module MdReplay = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReplay";
};

module MdReplay30 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReplay30";
};

module MdReplay5 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReplay5";
};

module MdShuffle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShuffle";
};

module MdSkipNext = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSkipNext";
};

module MdSkipPrevious = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSkipPrevious";
};

module MdSlowMotionVideo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSlowMotionVideo";
};

module MdSnooze = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSnooze";
};

module MdSortByAlpha = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSortByAlpha";
};

module MdStop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStop";
};

module MdSubscriptions = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSubscriptions";
};

module MdSubtitles = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSubtitles";
};

module MdSurroundSound = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSurroundSound";
};

module MdVideoCall = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVideoCall";
};

module MdVideoLabel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVideoLabel";
};

module MdVideoLibrary = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVideoLibrary";
};

module MdVideocam = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVideocam";
};

module MdVideocamOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVideocamOff";
};

module MdVolumeDown = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVolumeDown";
};

module MdVolumeMute = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVolumeMute";
};

module MdVolumeOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVolumeOff";
};

module MdVolumeUp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVolumeUp";
};

module MdWeb = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWeb";
};

module MdWebAsset = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWebAsset";
};

module MdBusiness = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBusiness";
};

module MdCall = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCall";
};

module MdCallEnd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallEnd";
};

module MdCallMade = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallMade";
};

module MdCallMerge = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallMerge";
};

module MdCallMissed = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallMissed";
};

module MdCallMissedOutgoing = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallMissedOutgoing";
};

module MdCallReceived = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallReceived";
};

module MdCallSplit = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCallSplit";
};

module MdChat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChat";
};

module MdChatBubble = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChatBubble";
};

module MdChatBubbleOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChatBubbleOutline";
};

module MdClearAll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdClearAll";
};

module MdComment = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdComment";
};

module MdContactMail = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdContactMail";
};

module MdContactPhone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdContactPhone";
};

module MdContacts = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdContacts";
};

module MdDialerSip = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDialerSip";
};

module MdDialpad = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDialpad";
};

module MdEmail = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEmail";
};

module MdForum = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdForum";
};

module MdImportContacts = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdImportContacts";
};

module MdImportExport = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdImportExport";
};

module MdInvertColorsOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInvertColorsOff";
};

module MdLiveHelp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLiveHelp";
};

module MdLocationOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocationOff";
};

module MdLocationOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocationOn";
};

module MdMailOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMailOutline";
};

module MdMessage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMessage";
};

module MdNoSim = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNoSim";
};

module MdPhone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhone";
};

module MdPhonelinkErase = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhonelinkErase";
};

module MdPhonelinkLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhonelinkLock";
};

module MdPhonelinkRing = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhonelinkRing";
};

module MdPhonelinkSetup = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhonelinkSetup";
};

module MdPortableWifiOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPortableWifiOff";
};

module MdPresentToAll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPresentToAll";
};

module MdRingVolume = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRingVolume";
};

module MdRssFeed = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRssFeed";
};

module MdScreenShare = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdScreenShare";
};

module MdSpeakerPhone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpeakerPhone";
};

module MdStayCurrentLandscape = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStayCurrentLandscape";
};

module MdStayCurrentPortrait = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStayCurrentPortrait";
};

module MdStayPrimaryLandscape = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStayPrimaryLandscape";
};

module MdStayPrimaryPortrait = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStayPrimaryPortrait";
};

module MdStopScreenShare = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStopScreenShare";
};

module MdSwapCalls = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSwapCalls";
};

module MdTextsms = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTextsms";
};

module MdVoicemail = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVoicemail";
};

module MdVpnKey = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVpnKey";
};

module MdAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAdd";
};

module MdAddBox = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddBox";
};

module MdAddCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddCircle";
};

module MdAddCircleOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddCircleOutline";
};

module MdArchive = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArchive";
};

module MdBackspace = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBackspace";
};

module MdBlock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBlock";
};

module MdClear = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdClear";
};

module MdContentCopy = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdContentCopy";
};

module MdContentCut = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdContentCut";
};

module MdContentPaste = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdContentPaste";
};

module MdCreate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCreate";
};

module MdDeleteSweep = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDeleteSweep";
};

module MdDrafts = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDrafts";
};

module MdFilterList = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterList";
};

module MdFlag = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlag";
};

module MdFontDownload = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFontDownload";
};

module MdForward = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdForward";
};

module MdGesture = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGesture";
};

module MdInbox = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInbox";
};

module MdLink = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLink";
};

module MdLowPriority = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLowPriority";
};

module MdMail = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMail";
};

module MdMarkunread = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMarkunread";
};

module MdMoveToInbox = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMoveToInbox";
};

module MdNextWeek = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNextWeek";
};

module MdRedo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRedo";
};

module MdRemove = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRemove";
};

module MdRemoveCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRemoveCircle";
};

module MdRemoveCircleOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRemoveCircleOutline";
};

module MdReply = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReply";
};

module MdReplyAll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReplyAll";
};

module MdReport = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdReport";
};

module MdSave = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSave";
};

module MdSelectAll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSelectAll";
};

module MdSend = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSend";
};

module MdSort = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSort";
};

module MdTextFormat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTextFormat";
};

module MdUnarchive = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdUnarchive";
};

module MdUndo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdUndo";
};

module MdWeekend = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWeekend";
};

module MdAccessAlarm = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccessAlarm";
};

module MdAccessAlarms = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccessAlarms";
};

module MdAccessTime = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAccessTime";
};

module MdAddAlarm = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddAlarm";
};

module MdAirplanemodeActive = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirplanemodeActive";
};

module MdAirplanemodeInactive = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirplanemodeInactive";
};

module MdBattery20 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBattery20";
};

module MdBattery30 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBattery30";
};

module MdBattery50 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBattery50";
};

module MdBattery60 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBattery60";
};

module MdBattery80 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBattery80";
};

module MdBattery90 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBattery90";
};

module MdBatteryAlert = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryAlert";
};

module MdBatteryCharging20 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryCharging20";
};

module MdBatteryCharging30 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryCharging30";
};

module MdBatteryCharging50 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryCharging50";
};

module MdBatteryCharging60 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryCharging60";
};

module MdBatteryCharging80 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryCharging80";
};

module MdBatteryCharging90 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryCharging90";
};

module MdBatteryChargingFull = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryChargingFull";
};

module MdBatteryFull = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryFull";
};

module MdBatteryStd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryStd";
};

module MdBatteryUnknown = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBatteryUnknown";
};

module MdBluetooth = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBluetooth";
};

module MdBluetoothConnected = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBluetoothConnected";
};

module MdBluetoothDisabled = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBluetoothDisabled";
};

module MdBluetoothSearching = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBluetoothSearching";
};

module MdBrightnessAuto = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightnessAuto";
};

module MdBrightnessHigh = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightnessHigh";
};

module MdBrightnessLow = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightnessLow";
};

module MdBrightnessMedium = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightnessMedium";
};

module MdDataUsage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDataUsage";
};

module MdDeveloperMode = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDeveloperMode";
};

module MdDevices = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDevices";
};

module MdDvr = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDvr";
};

module MdGpsFixed = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGpsFixed";
};

module MdGpsNotFixed = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGpsNotFixed";
};

module MdGpsOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGpsOff";
};

module MdGraphicEq = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGraphicEq";
};

module MdLocationDisabled = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocationDisabled";
};

module MdLocationSearching = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocationSearching";
};

module MdNetworkCell = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNetworkCell";
};

module MdNetworkWifi = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNetworkWifi";
};

module MdNfc = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNfc";
};

module MdScreenLockLandscape = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdScreenLockLandscape";
};

module MdScreenLockPortrait = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdScreenLockPortrait";
};

module MdScreenLockRotation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdScreenLockRotation";
};

module MdScreenRotation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdScreenRotation";
};

module MdSdStorage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSdStorage";
};

module MdSettingsSystemDaydream = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSettingsSystemDaydream";
};

module MdSignalCellular0Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellular0Bar";
};

module MdSignalCellular1Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellular1Bar";
};

module MdSignalCellular2Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellular2Bar";
};

module MdSignalCellular3Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellular3Bar";
};

module MdSignalCellular4Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellular4Bar";
};

module MdSignalCellularConnectedNoInternet0Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularConnectedNoInternet0Bar";
};

module MdSignalCellularConnectedNoInternet1Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularConnectedNoInternet1Bar";
};

module MdSignalCellularConnectedNoInternet2Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularConnectedNoInternet2Bar";
};

module MdSignalCellularConnectedNoInternet3Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularConnectedNoInternet3Bar";
};

module MdSignalCellularConnectedNoInternet4Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularConnectedNoInternet4Bar";
};

module MdSignalCellularNoSim = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularNoSim";
};

module MdSignalCellularNull = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularNull";
};

module MdSignalCellularOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalCellularOff";
};

module MdSignalWifi0Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi0Bar";
};

module MdSignalWifi1Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi1Bar";
};

module MdSignalWifi1BarLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi1BarLock";
};

module MdSignalWifi2Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi2Bar";
};

module MdSignalWifi2BarLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi2BarLock";
};

module MdSignalWifi3Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi3Bar";
};

module MdSignalWifi3BarLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi3BarLock";
};

module MdSignalWifi4Bar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi4Bar";
};

module MdSignalWifi4BarLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifi4BarLock";
};

module MdSignalWifiOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSignalWifiOff";
};

module MdStorage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStorage";
};

module MdUsb = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdUsb";
};

module MdWallpaper = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWallpaper";
};

module MdWidgets = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWidgets";
};

module MdWifiLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWifiLock";
};

module MdWifiTethering = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWifiTethering";
};

module MdAttachFile = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAttachFile";
};

module MdAttachMoney = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAttachMoney";
};

module MdBorderAll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderAll";
};

module MdBorderBottom = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderBottom";
};

module MdBorderClear = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderClear";
};

module MdBorderColor = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderColor";
};

module MdBorderHorizontal = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderHorizontal";
};

module MdBorderInner = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderInner";
};

module MdBorderLeft = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderLeft";
};

module MdBorderOuter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderOuter";
};

module MdBorderRight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderRight";
};

module MdBorderStyle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderStyle";
};

module MdBorderTop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderTop";
};

module MdBorderVertical = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBorderVertical";
};

module MdBubbleChart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBubbleChart";
};

module MdDragHandle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDragHandle";
};

module MdFormatAlignCenter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatAlignCenter";
};

module MdFormatAlignJustify = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatAlignJustify";
};

module MdFormatAlignLeft = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatAlignLeft";
};

module MdFormatAlignRight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatAlignRight";
};

module MdFormatBold = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatBold";
};

module MdFormatClear = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatClear";
};

module MdFormatColorFill = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatColorFill";
};

module MdFormatColorReset = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatColorReset";
};

module MdFormatColorText = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatColorText";
};

module MdFormatIndentDecrease = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatIndentDecrease";
};

module MdFormatIndentIncrease = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatIndentIncrease";
};

module MdFormatItalic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatItalic";
};

module MdFormatLineSpacing = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatLineSpacing";
};

module MdFormatListBulleted = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatListBulleted";
};

module MdFormatListNumbered = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatListNumbered";
};

module MdFormatPaint = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatPaint";
};

module MdFormatQuote = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatQuote";
};

module MdFormatShapes = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatShapes";
};

module MdFormatSize = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatSize";
};

module MdFormatStrikethrough = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatStrikethrough";
};

module MdFormatTextdirectionLToR = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatTextdirectionLToR";
};

module MdFormatTextdirectionRToL = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatTextdirectionRToL";
};

module MdFormatUnderlined = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFormatUnderlined";
};

module MdFunctions = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFunctions";
};

module MdHighlight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHighlight";
};

module MdInsertChart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInsertChart";
};

module MdInsertComment = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInsertComment";
};

module MdInsertDriveFile = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInsertDriveFile";
};

module MdInsertEmoticon = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInsertEmoticon";
};

module MdInsertInvitation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInsertInvitation";
};

module MdInsertLink = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInsertLink";
};

module MdInsertPhoto = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdInsertPhoto";
};

module MdLinearScale = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLinearScale";
};

module MdMergeType = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMergeType";
};

module MdModeComment = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdModeComment";
};

module MdModeEdit = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdModeEdit";
};

module MdMonetizationOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMonetizationOn";
};

module MdMoneyOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMoneyOff";
};

module MdMultilineChart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMultilineChart";
};

module MdPieChart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPieChart";
};

module MdPieChartOutlined = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPieChartOutlined";
};

module MdPublish = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPublish";
};

module MdShortText = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShortText";
};

module MdShowChart = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShowChart";
};

module MdSpaceBar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpaceBar";
};

module MdStrikethroughS = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStrikethroughS";
};

module MdTextFields = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTextFields";
};

module MdTitle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTitle";
};

module MdVerticalAlignBottom = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVerticalAlignBottom";
};

module MdVerticalAlignCenter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVerticalAlignCenter";
};

module MdVerticalAlignTop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVerticalAlignTop";
};

module MdWrapText = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWrapText";
};

module MdAttachment = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAttachment";
};

module MdCloud = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCloud";
};

module MdCloudCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCloudCircle";
};

module MdCloudDone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCloudDone";
};

module MdCloudDownload = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCloudDownload";
};

module MdCloudOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCloudOff";
};

module MdCloudQueue = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCloudQueue";
};

module MdCloudUpload = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCloudUpload";
};

module MdCreateNewFolder = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCreateNewFolder";
};

module MdFileDownload = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFileDownload";
};

module MdFileUpload = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFileUpload";
};

module MdFolder = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFolder";
};

module MdFolderOpen = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFolderOpen";
};

module MdFolderShared = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFolderShared";
};

module MdCast = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCast";
};

module MdCastConnected = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCastConnected";
};

module MdComputer = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdComputer";
};

module MdDesktopMac = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDesktopMac";
};

module MdDesktopWindows = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDesktopWindows";
};

module MdDeveloperBoard = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDeveloperBoard";
};

module MdDeviceHub = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDeviceHub";
};

module MdDevicesOther = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDevicesOther";
};

module MdDock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDock";
};

module MdGamepad = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGamepad";
};

module MdHeadset = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHeadset";
};

module MdHeadsetMic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHeadsetMic";
};

module MdKeyboard = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboard";
};

module MdKeyboardArrowDown = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardArrowDown";
};

module MdKeyboardArrowLeft = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardArrowLeft";
};

module MdKeyboardArrowRight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardArrowRight";
};

module MdKeyboardArrowUp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardArrowUp";
};

module MdKeyboardBackspace = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardBackspace";
};

module MdKeyboardCapslock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardCapslock";
};

module MdKeyboardHide = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardHide";
};

module MdKeyboardReturn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardReturn";
};

module MdKeyboardTab = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardTab";
};

module MdKeyboardVoice = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKeyboardVoice";
};

module MdLaptop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLaptop";
};

module MdLaptopChromebook = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLaptopChromebook";
};

module MdLaptopMac = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLaptopMac";
};

module MdLaptopWindows = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLaptopWindows";
};

module MdMemory = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMemory";
};

module MdMouse = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMouse";
};

module MdPhoneAndroid = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoneAndroid";
};

module MdPhoneIphone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoneIphone";
};

module MdPhonelink = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhonelink";
};

module MdPhonelinkOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhonelinkOff";
};

module MdPowerInput = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPowerInput";
};

module MdRouter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRouter";
};

module MdScanner = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdScanner";
};

module MdSecurity = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSecurity";
};

module MdSimCard = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSimCard";
};

module MdSmartphone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSmartphone";
};

module MdSpeaker = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpeaker";
};

module MdSpeakerGroup = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpeakerGroup";
};

module MdTablet = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTablet";
};

module MdTabletAndroid = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTabletAndroid";
};

module MdTabletMac = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTabletMac";
};

module MdToys = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdToys";
};

module MdTv = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTv";
};

module MdVideogameAsset = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVideogameAsset";
};

module MdWatch = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWatch";
};

module MdAddAPhoto = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddAPhoto";
};

module MdAddToPhotos = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddToPhotos";
};

module MdAdjust = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAdjust";
};

module MdAssistant = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssistant";
};

module MdAssistantPhoto = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAssistantPhoto";
};

module MdAudiotrack = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAudiotrack";
};

module MdBlurCircular = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBlurCircular";
};

module MdBlurLinear = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBlurLinear";
};

module MdBlurOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBlurOff";
};

module MdBlurOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBlurOn";
};

module MdBrightness1 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightness1";
};

module MdBrightness2 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightness2";
};

module MdBrightness3 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightness3";
};

module MdBrightness4 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightness4";
};

module MdBrightness5 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightness5";
};

module MdBrightness6 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightness6";
};

module MdBrightness7 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrightness7";
};

module MdBrokenImage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrokenImage";
};

module MdBrush = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBrush";
};

module MdBurstMode = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBurstMode";
};

module MdCamera = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCamera";
};

module MdCameraAlt = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCameraAlt";
};

module MdCameraFront = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCameraFront";
};

module MdCameraRear = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCameraRear";
};

module MdCameraRoll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCameraRoll";
};

module MdCenterFocusStrong = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCenterFocusStrong";
};

module MdCenterFocusWeak = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCenterFocusWeak";
};

module MdCollections = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCollections";
};

module MdCollectionsBookmark = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCollectionsBookmark";
};

module MdColorLens = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdColorLens";
};

module MdColorize = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdColorize";
};

module MdCompare = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCompare";
};

module MdControlPoint = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdControlPoint";
};

module MdControlPointDuplicate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdControlPointDuplicate";
};

module MdCrop169 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCrop169";
};

module MdCrop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCrop";
};

module MdCrop32 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCrop32";
};

module MdCrop54 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCrop54";
};

module MdCrop75 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCrop75";
};

module MdCropDin = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCropDin";
};

module MdCropFree = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCropFree";
};

module MdCropLandscape = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCropLandscape";
};

module MdCropOriginal = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCropOriginal";
};

module MdCropPortrait = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCropPortrait";
};

module MdCropRotate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCropRotate";
};

module MdCropSquare = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCropSquare";
};

module MdDehaze = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDehaze";
};

module MdDetails = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDetails";
};

module MdEdit = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEdit";
};

module MdExposure = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExposure";
};

module MdExposureNeg1 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExposureNeg1";
};

module MdExposureNeg2 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExposureNeg2";
};

module MdExposurePlus1 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExposurePlus1";
};

module MdExposurePlus2 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExposurePlus2";
};

module MdExposureZero = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExposureZero";
};

module MdFilter1 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter1";
};

module MdFilter2 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter2";
};

module MdFilter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter";
};

module MdFilter3 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter3";
};

module MdFilter4 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter4";
};

module MdFilter5 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter5";
};

module MdFilter6 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter6";
};

module MdFilter7 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter7";
};

module MdFilter8 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter8";
};

module MdFilter9 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter9";
};

module MdFilter9Plus = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilter9Plus";
};

module MdFilterBAndW = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterBAndW";
};

module MdFilterCenterFocus = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterCenterFocus";
};

module MdFilterDrama = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterDrama";
};

module MdFilterFrames = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterFrames";
};

module MdFilterHdr = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterHdr";
};

module MdFilterNone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterNone";
};

module MdFilterTiltShift = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterTiltShift";
};

module MdFilterVintage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFilterVintage";
};

module MdFlare = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlare";
};

module MdFlashAuto = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlashAuto";
};

module MdFlashOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlashOff";
};

module MdFlashOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlashOn";
};

module MdFlip = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlip";
};

module MdGradient = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGradient";
};

module MdGrain = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGrain";
};

module MdGridOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGridOff";
};

module MdGridOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGridOn";
};

module MdHdrOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHdrOff";
};

module MdHdrOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHdrOn";
};

module MdHdrStrong = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHdrStrong";
};

module MdHdrWeak = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHdrWeak";
};

module MdHealing = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHealing";
};

module MdImage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdImage";
};

module MdImageAspectRatio = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdImageAspectRatio";
};

module MdIso = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdIso";
};

module MdLandscape = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLandscape";
};

module MdLeakAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLeakAdd";
};

module MdLeakRemove = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLeakRemove";
};

module MdLens = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLens";
};

module MdLinkedCamera = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLinkedCamera";
};

module MdLooks = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLooks";
};

module MdLooks3 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLooks3";
};

module MdLooks4 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLooks4";
};

module MdLooks5 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLooks5";
};

module MdLooks6 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLooks6";
};

module MdLooksOne = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLooksOne";
};

module MdLooksTwo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLooksTwo";
};

module MdLoupe = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLoupe";
};

module MdMonochromePhotos = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMonochromePhotos";
};

module MdMovieCreation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMovieCreation";
};

module MdMovieFilter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMovieFilter";
};

module MdMusicNote = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMusicNote";
};

module MdNature = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNature";
};

module MdNaturePeople = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNaturePeople";
};

module MdNavigateBefore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNavigateBefore";
};

module MdNavigateNext = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNavigateNext";
};

module MdPalette = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPalette";
};

module MdPanorama = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPanorama";
};

module MdPanoramaFishEye = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPanoramaFishEye";
};

module MdPanoramaHorizontal = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPanoramaHorizontal";
};

module MdPanoramaVertical = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPanoramaVertical";
};

module MdPanoramaWideAngle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPanoramaWideAngle";
};

module MdPhoto = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoto";
};

module MdPhotoAlbum = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhotoAlbum";
};

module MdPhotoCamera = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhotoCamera";
};

module MdPhotoFilter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhotoFilter";
};

module MdPhotoLibrary = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhotoLibrary";
};

module MdPhotoSizeSelectActual = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhotoSizeSelectActual";
};

module MdPhotoSizeSelectLarge = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhotoSizeSelectLarge";
};

module MdPhotoSizeSelectSmall = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhotoSizeSelectSmall";
};

module MdPictureAsPdf = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPictureAsPdf";
};

module MdPortrait = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPortrait";
};

module MdRemoveRedEye = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRemoveRedEye";
};

module MdRotate90DegreesCcw = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRotate90DegreesCcw";
};

module MdRotateLeft = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRotateLeft";
};

module MdRotateRight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRotateRight";
};

module MdSlideshow = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSlideshow";
};

module MdStraighten = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStraighten";
};

module MdStyle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStyle";
};

module MdSwitchCamera = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSwitchCamera";
};

module MdSwitchVideo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSwitchVideo";
};

module MdTagFaces = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTagFaces";
};

module MdTexture = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTexture";
};

module MdTimelapse = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTimelapse";
};

module MdTimer10 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTimer10";
};

module MdTimer = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTimer";
};

module MdTimer3 = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTimer3";
};

module MdTimerOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTimerOff";
};

module MdTonality = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTonality";
};

module MdTransform = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTransform";
};

module MdTune = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTune";
};

module MdViewComfy = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewComfy";
};

module MdViewCompact = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdViewCompact";
};

module MdVignette = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVignette";
};

module MdWbAuto = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWbAuto";
};

module MdWbCloudy = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWbCloudy";
};

module MdWbIncandescent = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWbIncandescent";
};

module MdWbIridescent = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWbIridescent";
};

module MdWbSunny = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWbSunny";
};

module MdAddLocation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAddLocation";
};

module MdBeenhere = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBeenhere";
};

module MdDirections = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirections";
};

module MdDirectionsBike = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsBike";
};

module MdDirectionsBoat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsBoat";
};

module MdDirectionsBus = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsBus";
};

module MdDirectionsCar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsCar";
};

module MdDirectionsRailway = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsRailway";
};

module MdDirectionsRun = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsRun";
};

module MdDirectionsSubway = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsSubway";
};

module MdDirectionsTransit = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsTransit";
};

module MdDirectionsWalk = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDirectionsWalk";
};

module MdEditLocation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEditLocation";
};

module MdEvStation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEvStation";
};

module MdFlight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFlight";
};

module MdHotel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHotel";
};

module MdLayers = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLayers";
};

module MdLayersClear = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLayersClear";
};

module MdLocalActivity = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalActivity";
};

module MdLocalAirport = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalAirport";
};

module MdLocalAtm = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalAtm";
};

module MdLocalBar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalBar";
};

module MdLocalCafe = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalCafe";
};

module MdLocalCarWash = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalCarWash";
};

module MdLocalConvenienceStore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalConvenienceStore";
};

module MdLocalDining = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalDining";
};

module MdLocalDrink = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalDrink";
};

module MdLocalFlorist = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalFlorist";
};

module MdLocalGasStation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalGasStation";
};

module MdLocalGroceryStore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalGroceryStore";
};

module MdLocalHospital = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalHospital";
};

module MdLocalHotel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalHotel";
};

module MdLocalLaundryService = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalLaundryService";
};

module MdLocalLibrary = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalLibrary";
};

module MdLocalMall = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalMall";
};

module MdLocalMovies = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalMovies";
};

module MdLocalOffer = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalOffer";
};

module MdLocalParking = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalParking";
};

module MdLocalPharmacy = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalPharmacy";
};

module MdLocalPhone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalPhone";
};

module MdLocalPizza = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalPizza";
};

module MdLocalPlay = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalPlay";
};

module MdLocalPostOffice = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalPostOffice";
};

module MdLocalPrintshop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalPrintshop";
};

module MdLocalSee = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalSee";
};

module MdLocalShipping = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalShipping";
};

module MdLocalTaxi = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocalTaxi";
};

module MdMap = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMap";
};

module MdMyLocation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMyLocation";
};

module MdNavigation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNavigation";
};

module MdNearMe = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNearMe";
};

module MdPersonPin = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPersonPin";
};

module MdPersonPinCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPersonPinCircle";
};

module MdPinDrop = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPinDrop";
};

module MdPlace = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlace";
};

module MdRateReview = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRateReview";
};

module MdRestaurant = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRestaurant";
};

module MdRestaurantMenu = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRestaurantMenu";
};

module MdSatellite = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSatellite";
};

module MdStoreMallDirectory = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStoreMallDirectory";
};

module MdStreetview = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStreetview";
};

module MdSubway = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSubway";
};

module MdTerrain = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTerrain";
};

module MdTraffic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTraffic";
};

module MdTrain = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTrain";
};

module MdTram = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTram";
};

module MdTransferWithinAStation = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTransferWithinAStation";
};

module MdZoomOutMap = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdZoomOutMap";
};

module MdApps = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdApps";
};

module MdArrowBack = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArrowBack";
};

module MdArrowDownward = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArrowDownward";
};

module MdArrowDropDown = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArrowDropDown";
};

module MdArrowDropDownCircle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArrowDropDownCircle";
};

module MdArrowDropUp = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArrowDropUp";
};

module MdArrowForward = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArrowForward";
};

module MdArrowUpward = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdArrowUpward";
};

module MdCancel = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCancel";
};

module MdCheck = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCheck";
};

module MdChevronLeft = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChevronLeft";
};

module MdChevronRight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChevronRight";
};

module MdClose = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdClose";
};

module MdExpandLess = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExpandLess";
};

module MdExpandMore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdExpandMore";
};

module MdFirstPage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFirstPage";
};

module MdFullscreen = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFullscreen";
};

module MdFullscreenExit = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFullscreenExit";
};

module MdLastPage = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLastPage";
};

module MdMenu = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMenu";
};

module MdMoreHoriz = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMoreHoriz";
};

module MdMoreVert = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMoreVert";
};

module MdRefresh = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRefresh";
};

module MdSubdirectoryArrowLeft = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSubdirectoryArrowLeft";
};

module MdSubdirectoryArrowRight = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSubdirectoryArrowRight";
};

module MdUnfoldLess = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdUnfoldLess";
};

module MdUnfoldMore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdUnfoldMore";
};

module MdAdb = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAdb";
};

module MdAirlineSeatFlat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatFlat";
};

module MdAirlineSeatFlatAngled = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatFlatAngled";
};

module MdAirlineSeatIndividualSuite = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatIndividualSuite";
};

module MdAirlineSeatLegroomExtra = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatLegroomExtra";
};

module MdAirlineSeatLegroomNormal = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatLegroomNormal";
};

module MdAirlineSeatLegroomReduced = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatLegroomReduced";
};

module MdAirlineSeatReclineExtra = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatReclineExtra";
};

module MdAirlineSeatReclineNormal = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirlineSeatReclineNormal";
};

module MdBluetoothAudio = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBluetoothAudio";
};

module MdConfirmationNumber = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdConfirmationNumber";
};

module MdDiscFull = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDiscFull";
};

module MdDoNotDisturb = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDoNotDisturb";
};

module MdDoNotDisturbAlt = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDoNotDisturbAlt";
};

module MdDoNotDisturbOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDoNotDisturbOff";
};

module MdDoNotDisturbOn = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDoNotDisturbOn";
};

module MdDriveEta = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDriveEta";
};

module MdEnhancedEncryption = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEnhancedEncryption";
};

module MdEventAvailable = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEventAvailable";
};

module MdEventBusy = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEventBusy";
};

module MdEventNote = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdEventNote";
};

module MdFolderSpecial = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFolderSpecial";
};

module MdLiveTv = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLiveTv";
};

module MdMms = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMms";
};

module MdMore = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMore";
};

module MdNetworkCheck = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNetworkCheck";
};

module MdNetworkLocked = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNetworkLocked";
};

module MdNoEncryption = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNoEncryption";
};

module MdOndemandVideo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdOndemandVideo";
};

module MdPersonalVideo = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPersonalVideo";
};

module MdPhoneBluetoothSpeaker = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoneBluetoothSpeaker";
};

module MdPhoneForwarded = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoneForwarded";
};

module MdPhoneInTalk = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoneInTalk";
};

module MdPhoneLocked = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoneLocked";
};

module MdPhoneMissed = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhoneMissed";
};

module MdPhonePaused = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPhonePaused";
};

module MdPower = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPower";
};

module MdPriorityHigh = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPriorityHigh";
};

module MdRvHookup = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRvHookup";
};

module MdSdCard = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSdCard";
};

module MdSimCardAlert = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSimCardAlert";
};

module MdSms = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSms";
};

module MdSmsFailed = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSmsFailed";
};

module MdSync = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSync";
};

module MdSyncDisabled = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSyncDisabled";
};

module MdSyncProblem = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSyncProblem";
};

module MdSystemUpdate = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSystemUpdate";
};

module MdTapAndPlay = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTapAndPlay";
};

module MdTimeToLeave = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdTimeToLeave";
};

module MdVibration = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVibration";
};

module MdVoiceChat = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVoiceChat";
};

module MdVpnLock = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdVpnLock";
};

module MdWc = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWc";
};

module MdWifi = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWifi";
};

module MdAcUnit = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAcUnit";
};

module MdAirportShuttle = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAirportShuttle";
};

module MdAllInclusive = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdAllInclusive";
};

module MdBeachAccess = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBeachAccess";
};

module MdBusinessCenter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdBusinessCenter";
};

module MdCasino = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCasino";
};

module MdChildCare = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChildCare";
};

module MdChildFriendly = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdChildFriendly";
};

module MdFitnessCenter = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFitnessCenter";
};

module MdFreeBreakfast = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdFreeBreakfast";
};

module MdGolfCourse = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGolfCourse";
};

module MdHotTub = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdHotTub";
};

module MdKitchen = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdKitchen";
};

module MdPool = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPool";
};

module MdRoomService = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRoomService";
};

module MdSmokeFree = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSmokeFree";
};

module MdSmokingRooms = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSmokingRooms";
};

module MdSpa = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSpa";
};

module MdCake = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCake";
};

module MdDomain = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdDomain";
};

module MdGroup = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGroup";
};

module MdGroupAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdGroupAdd";
};

module MdLocationCity = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdLocationCity";
};

module MdMood = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMood";
};

module MdMoodBad = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdMoodBad";
};

module MdNotifications = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNotifications";
};

module MdNotificationsActive = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNotificationsActive";
};

module MdNotificationsNone = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNotificationsNone";
};

module MdNotificationsOff = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNotificationsOff";
};

module MdNotificationsPaused = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdNotificationsPaused";
};

module MdPages = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPages";
};

module MdPartyMode = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPartyMode";
};

module MdPeople = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPeople";
};

module MdPeopleOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPeopleOutline";
};

module MdPerson = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPerson";
};

module MdPersonAdd = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPersonAdd";
};

module MdPersonOutline = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPersonOutline";
};

module MdPlusOne = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPlusOne";
};

module MdPoll = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPoll";
};

module MdPublic = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdPublic";
};

module MdSchool = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSchool";
};

module MdSentimentDissatisfied = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSentimentDissatisfied";
};

module MdSentimentNeutral = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSentimentNeutral";
};

module MdSentimentSatisfied = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSentimentSatisfied";
};

module MdSentimentVeryDissatisfied = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSentimentVeryDissatisfied";
};

module MdSentimentVerySatisfied = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdSentimentVerySatisfied";
};

module MdShare = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdShare";
};

module MdWhatshot = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdWhatshot";
};

module MdCheckBox = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCheckBox";
};

module MdCheckBoxOutlineBlank = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdCheckBoxOutlineBlank";
};

module MdIndeterminateCheckBox = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdIndeterminateCheckBox";
};

module MdRadioButtonChecked = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRadioButtonChecked";
};

module MdRadioButtonUnchecked = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdRadioButtonUnchecked";
};

module MdStar = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStar";
};

module MdStarBorder = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStarBorder";
};

module MdStarHalf = {
  [@bs.module "react-icons/md"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "MdStarHalf";
};

module FaFlagCheckered = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaFlagCheckered";
};

module FaArrowAltCircleRight = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaArrowAltCircleRight";
};
module FaArrowAltCircleLeft = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaArrowAltCircleLeft";
};

module FaArrowsAltV = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaArrowsAltV";
};

module FaInfoCircle = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaInfoCircle";
};

module FaInfo = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaInfo";
};

module FiExternalLink = {
  [@bs.module "react-icons/fi"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FiExternalLink";
};

module FaMapMarkerAlt = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaMapMarkerAlt";
};

module FaCalendarAlt = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaCalendarAlt";
};

module FaStar = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaStar";
};

module FaCircle = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaCircle";
};

module FaFileExcel = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaFileExcel";
};

module FaBuilding = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaBuilding";
};

module FaFileInvoiceDollar = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaFileInvoiceDollar";
};
module FaUsers = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaUsers";
};
module FaFilePdf = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaFilePdf";
};
module FaSortUp = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaSortUp";
};
module FaSort = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaSort";
};
module FaSortDown = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaSortDown";
};

module GrElevator = {
  [@bs.module "react-icons/gr"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "GrElevator";
};

module FaLevelUpAlt = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaLevelUpAlt";
};

module IoIosArrowBack = {
  [@bs.module "react-icons/io"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "IoIosArrowBack";
};

module IoIosArrowForward = {
  [@bs.module "react-icons/io"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "IoIosArrowForward";
};

module FaCheck = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaCheck";
};

module FaExclamation = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaExclamation";
};

module FaMap = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaMap";
};

module GrFormDown = {
  [@bs.module "react-icons/gr"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "GrFormDown";
};

module FaTruck = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaTruck";
};

module FcViewDetails = {
  [@bs.module "react-icons/fc"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FcViewDetails";
};

module RiCalendarEventLine = {
  [@bs.module "react-icons/ri"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "RiCalendarEventLine";
};

module FaStore = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaStore";
};

module FaClock = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaClock";
};

module FaRoute = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaRoute";
};

module FaPencilAlt = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaPencilAlt";
};

module FaBoxOpen = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaBoxOpen";
};
module FaThumbsUp = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaThumbsUp";
};
module FaAngleDown = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaAngleDown";
};
module FaRegStar = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaRegStar";
};
module IoIosWarning = {
  [@bs.module "react-icons/io"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "IoIosWarning";
};

module FaBook = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaBook";
};

module FaCalendarTimes = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaCalendarTimes";
};

module FaQuestionCircle = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaQuestionCircle";
};

module FaEuroSign = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaEuroSign";
};

module FaSlidersH = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaSlidersH";
};

module FaExclamationCircle = {
  [@bs.module "react-icons/fa"] [@react.component]
  external make:
    (~size: int=?, ~color: string=?, ~className: string=?) => React.element =
    "FaExclamationCircle";
};
