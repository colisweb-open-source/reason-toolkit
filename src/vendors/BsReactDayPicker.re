module ReactDayPicker = {
  type range = {
    from: Js.Nullable.t(Js.Date.t),
    [@bs.as "to"]
    to_: Js.Nullable.t(Js.Date.t),
  };

  type modifier = {
    start: Js.Nullable.t(Js.Date.t),
    [@bs.as "end"]
    end_: Js.Nullable.t(Js.Date.t),
  };

  module DateUtils = {
    [@bs.module "react-day-picker"] [@bs.scope "DateUtils"]
    external addDayToRange: (Js.Date.t, range) => range = "addDayToRange";
  };

  type selectedDays;
  type disabledDays;

  external toSelectedDays: 'a => selectedDays = "%identity";

  let makeSelectedDays = (range: range) =>
    [|range.from, range->Obj.magic|]->toSelectedDays;

  [@bs.obj]
  external makeDisabledDays:
    (~after: option(Js.Date.t)=?, ~before: option(Js.Date.t)=?, unit) =>
    disabledDays;

  type dayPickerProps;

  [@bs.obj]
  external makeDayPickerProps:
    (
      ~selectedDays: selectedDays=?,
      ~disabledDays: disabledDays=?,
      ~toMonth: option(Js.Date.t)=?,
      ~fromMonth: option(Js.Date.t)=?,
      ~month: option(Js.Date.t)=?,
      ~modifiers: modifier,
      ~numberOfMonths: int,
      ~onDayClick: unit => unit=?,
      ~locale: string=?,
      ~weekdaysShort: 'a=?,
      ~months: 'a=?,
      unit
    ) =>
    dayPickerProps;

  type classnames = {
    container: string,
    overlayWrapper: string,
    overlay: string,
  };

  module DayPickerInput = {
    [@bs.module "react-day-picker/DayPickerInput"] [@react.component]
    external make:
      (
        ~formatDate: Js.Date.t => string=?,
        ~format: string=?,
        ~placeholder: string=?,
        ~value: Js.Date.t=?,
        ~onDayChange: Js.Date.t => unit=?,
        ~selectedDays: (Js.Date.t, range)=?,
        ~modifiers: modifier=?,
        ~classNames: classnames=?,
        ~locale: string=?,
        ~weekdaysShort: 'a=?,
        ~firstDayOfWeek: 'b=?,
        ~months: 'a=?,
        ~showOutsideDays: bool=?,
        ~dayPickerProps: dayPickerProps=?,
        ~formatDate: Js.Date.t => string=?,
        ~ref: ReactDOMRe.Ref.t=?
      ) =>
      React.element =
      "default";
  };
  module DayPicker = {
    [@bs.module "react-day-picker/DayPicker"] [@react.component]
    external make:
      (
        ~formatDate: Js.Date.t => string=?,
        ~format: string=?,
        ~placeholder: string=?,
        ~value: string=?,
        ~onDayClick: Js.Date.t => unit=?,
        ~selectedDays: (Js.Date.t, range)=?,
        ~modifiers: modifier=?,
        ~className: string=?,
        ~disabledDays: disabledDays=?,
        ~locale: string=?,
        ~weekdaysShort: 'a=?,
        ~firstDayOfWeek: 'b=?,
        ~months: 'a=?,
        ~numberOfMonths: int=?,
        ~showOutsideDays: bool=?
      ) =>
      React.element =
      "default";
  };
};
