module Sentry = {
  // init
  type initOptions;

  [@bs.obj]
  external initOptions:
    (
      ~dsn: string,
      ~environment: string,
      ~beforeSend: ('a, 'b) => 'c=?,
      unit
    ) =>
    initOptions;

  [@bs.module "@sentry/browser"] external init: initOptions => unit = "init";

  // setTag
  [@bs.module "@sentry/browser"]
  external setTag: (string, string) => unit = "setTag";

  // addBreadcrumb
  type breadcrumbOptions;

  [@bs.obj]
  external breadcrumbOptions:
    (
      ~data: option(Js.t('a)),
      ~category: string,
      ~message: string,
      ~level: [ | `fatal | `error | `warning | `info | `debug]
    ) =>
    breadcrumbOptions;

  [@bs.module "@sentry/browser"]
  external addBreadcrumb: breadcrumbOptions => unit = "addBreadcrumb";

  // setUser
  [@bs.module "@sentry/browser"] external setUser: 'user => unit = "setUser";

  module Scope = {
    type t;

    [@bs.send] external setExtra: (t, string, 'a) => unit = "setExtra";
    [@bs.send] external setTag: (t, string, 'a) => unit = "setTag";
    [@bs.send] external setUser: (t, 'a) => unit = "setUser";
  };

  // withScope
  [@bs.module "@sentry/browser"]
  external withScope: (Scope.t => unit) => unit = "withScope";

  [@bs.module "@sentry/browser"]
  external configureScope: (Scope.t => unit) => unit = "configureScope";

  // captureException
  [@bs.module "@sentry/browser"]
  external captureException: Js.Promise.error => unit = "captureException";

  // captureMessage
  [@bs.module "@sentry/browser"]
  external captureMessage: string => unit = "captureMessage";
};
