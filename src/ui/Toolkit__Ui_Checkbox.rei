type size = [ | `xs | `sm | `md | `lg];

[@react.component]
let make:
  (
    ~value: string,
    ~children: React.element=?,
    ~disabled: bool=?,
    ~onChange: (bool, string) => unit=?,
    ~name: string=?,
    ~checked: bool=?,
    ~className: string=?,
    ~size: size=?
  ) =>
  React.element;
