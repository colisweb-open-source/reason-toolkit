# UI

Set of components using Tailwind

## Usage

Import the CSS style in your app :

```js
// App.js
import '@colisweb/reason-toolkit/src/ui/styles.css
```

```reasonml
open Toolkit.Ui;

[@react.component]
let make = () => {
  <div>
    <Button>"Test"->React.string</Button>
  </div>
}
```
