type color = [
  | `black
  | `white
  | `gray
  | `primary
  | `info
  | `danger
  | `warning
  | `success
  | `neutral
];

type size = [ | `sm | `md | `lg | `xs];
type variant = [ | `plain | `outline];

[@react.component]
let make =
    (~color=`gray, ~size=`md, ~className="", ~variant=`plain, ~children) => {
  let sizeStyle =
    fun
    | `xs => "text-xs leading-4 px-1"
    | `sm => "text-xs leading-6 px-2"
    | `md => "text-sm leading-8 px-3"
    | `lg => "text-base leading-10 px-4";

  let colorStyle = c =>
    switch (c, variant) {
    | (c, `plain) =>
      switch (c) {
      | `black => "bg-gray-900 text-white"
      | `white => "bg-white text-gray-900"
      | `gray => "bg-gray-200 text-gray-800"
      | `primary => "bg-primary-500 text-white"
      | `info => "bg-info-600 text-white"
      | `danger => "bg-danger-600 text-white"
      | `warning => "bg-warning-50 text-warning-700"
      | `success => "bg-success-50 text-success-700 bg-opacity-50"
      | `neutral => "bg-gray-100 text-neutral-600"
      }
    | (c, `outline) =>
      switch (c) {
      | `black => "border border-gray-900 text-white"
      | `white => "border border-white text-gray-900"
      | `gray => "border border-gray-400 text-gray-700"
      | `primary => "border border-primary-500 text-white"
      | `info => "border border-info-600 text-white"
      | `danger => "border border-danger-600 text-white"
      | `warning => "border text-warning-700 border-warning-700"
      | `success => "border text-success-700 border-success-700"
      | `neutral => "border text-neutral-600 border-neutral-600"
      }
    };

  <div
    className={Cn.make([
      className,
      "inline-flex self-center rounded font-semibold max-w-xs",
      colorStyle(color),
      sizeStyle(size),
    ])}>
    <span className="truncate"> children </span>
  </div>;
};
