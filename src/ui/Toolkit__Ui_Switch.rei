type size = [ | `md | `lg];

[@react.component]
let make:
  (
    ~onChange: bool => unit=?,
    ~name: string=?,
    ~checked: bool=?,
    ~size: size=?
  ) =>
  React.element;
