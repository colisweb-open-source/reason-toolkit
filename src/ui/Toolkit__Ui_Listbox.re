module ListboxPopover = {
  [@react.component]
  let make =
    React.forwardRef(
      (
        ~onClick: option(ReactEvent.Mouse.t => unit)=?,
        ~isOpen=false,
        ~className="",
        ~children: React.element,
        ref_,
      ) => {
      <div
        ?onClick
        ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
        className={Cn.make([
          className,
          "absolute bg-white z-40 shadow-lg rounded",
          "border py-2 mt-1"->Cn.ifTrue(isOpen),
        ])}>
        children
      </div>
    });
};

module ListboxList = {
  [@react.component]
  let make =
    React.forwardRef(
      (
        ~isOpen=false,
        ~ariaLabelledby: option(string)=?,
        ~ariaActivedescendant: option(string)=?,
        ~role: option(string)=?,
        ~id: option(string)=?,
        ~onMouseLeave: option(ReactEvent.Mouse.t => unit)=?,
        ~onKeyDown: option(ReactEvent.Keyboard.t => unit)=?,
        ~onBlur: option(ReactEvent.Focus.t => unit)=?,
        ~tabIndex: option(int)=?,
        ~className="",
        ~children: React.element,
        ref_,
      ) => {
      <ul
        ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
        ?ariaLabelledby
        ?ariaActivedescendant
        ?role
        ?id
        ?onMouseLeave
        ?onKeyDown
        ?onBlur
        ?tabIndex
        className={Cn.make([
          className,
          "focus:outline-none focus:shadow-none max-h-sm overflow-y-auto",
        ])}>
        {isOpen ? children : React.null}
      </ul>
    });
};

module ListboxOption = {
  [@react.component]
  let make =
    React.forwardRef(
      (
        ~ariaSelected: option(bool)=?,
        ~id: option(string)=?,
        ~role: option(string)=?,
        ~onClick: option(ReactEvent.Mouse.t => unit)=?,
        ~onMouseMove: option(ReactEvent.Mouse.t => unit)=?,
        ~onMouseDown: option(ReactEvent.Mouse.t => unit)=?,
        ~disabled: option(bool)=?,
        ~isHighlighted: option(bool)=?,
        ~className="",
        ~children: React.element,
        ref_,
      ) => {
      <li
        ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
        ?ariaSelected
        ?id
        ?role
        ?onClick
        ?onMouseMove
        ?onMouseDown
        ?disabled
        className={Cn.make([
          className,
          "py-2 px-4 flex items-center select-none transition duration-150 ease-in-out",
          "bg-gray-200"
          ->Cn.ifTrue(isHighlighted->Option.getWithDefault(false)),
          "text-gray-600"->Cn.ifTrue(disabled->Option.getWithDefault(false)),
        ])}>
        children
      </li>
    });
};

module ListboxGroupLabel = {
  [@react.component]
  let make =
    React.forwardRef((~className="", ~children: React.element, ref_) => {
      <div
        role="group"
        ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
        className={Cn.make([
          className,
          "py-2 px-4 flex items-center select-none text-sm font-bold",
        ])}>
        children
      </div>
    });
};

module ListboxLabel = {
  [@react.component]
  let make =
    React.forwardRef((~className="", ~children: React.element, ref_) => {
      <div
        ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
        className={Cn.make([
          className,
          "py-2 px-4 flex items-center select-none",
        ])}>
        children
      </div>
    });
};

module ListboxDivider = {
  [@react.component]
  let make = (~className="") => {
    <div className={Cn.make([className, "pb-2 mb-2 border-b"])} />;
  };
};

module Popover = ListboxPopover;
module List = ListboxList;
module Option = ListboxOption;
module Label = ListboxLabel;
module GroupLabel = ListboxGroupLabel;
module Divider = ListboxDivider;
