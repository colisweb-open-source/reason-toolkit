open BsDownshift;
module Listbox = Toolkit__Ui_Listbox;

module ButtonInput = {
  module Button = Toolkit__Ui_Button;
  type size = Button.size;

  [@react.component]
  let make =
    React.forwardRef(
      (
        ~size=`md,
        ~ariaLabel: option(string)=?,
        ~ariaLabelledby: option(string)=?,
        ~ariaExpanded: option(bool)=?,
        ~id: option(string)=?,
        ~type_="button",
        ~onClick: option(ReactEvent.Mouse.t => unit)=?,
        ~disabled=false,
        ~isInvalid: option(bool)=?,
        ~autoFocus: option(bool)=?,
        ~value=?,
        ~className="",
        ~children=React.null,
        ref_,
      ) => {
      <Button
        ref=ref_
        size
        color=`white
        variant=`default
        type_
        ?ariaLabel
        ?ariaLabelledby
        ?ariaExpanded
        ?id
        ?onClick
        disabled
        ?autoFocus
        className={Cn.make([
          className,
          "w-full flex justify-between",
          "border-danger-500 shadow-danger-500"
          ->Cn.ifTrue(isInvalid->Option.getWithDefault(false)),
        ])}>
        {value->Option.mapWithDefault(React.null, value =>
           <span className="font-normal truncate"> value->React.string </span>
         )}
        children
      </Button>
    });
};

module Item = {
  type value;

  external fromValue: value => 'any = "%identity";

  external toValue: 'any => value = "%identity";

  type item =
    | Placeholder
    | Item(string, value);

  let item = (key, value) => Item(key, value->toValue);

  let mapItem = (item, fn) =>
    switch (item) {
    | Placeholder => None
    | Item(key, value) => Some(fn((key, value->fromValue)))
    };

  let value = item =>
    switch (item) {
    | Placeholder => None
    | Item(_, value) => Some(value->fromValue)
    };
};

include Item;

type selection = {
  selection: option(item),
  selectItem: item => unit,
  isSelected: item => bool,
};

let isEqualItems = (itemA, itemB) => {
  switch (itemA, itemB) {
  | (Placeholder, Placeholder) => true
  | (Item(keyA, _), Item(keyB, _)) when keyA === keyB => true
  | _ => false
  };
};

let getSelectionByValue = (items, value) => {
  items->Array.getBy(isEqualItems(value))->Option.getExn;
};

let useSelection =
    (
      ~value: option(item)=?,
      ~defaultValue: option(item)=?,
      ~onChange: option(option(item) => unit)=?,
      ~items: array(item),
    )
    : selection => {
  let isControlled = value->Option.isSome;

  let (selection, setSelection) =
    React.useState(_ =>
      defaultValue->Option.map(getSelectionByValue(items))
    );

  let selection =
    !isControlled
      ? selection : value->Option.map(getSelectionByValue(items));

  let setSelection = !isControlled ? setSelection : (fn => fn(None)->ignore);

  let selectItem =
    React.useCallback1(
      selectedItem => {
        let newSelection =
          switch (selection, selectedItem) {
          | (Some(selection), selectedItem)
              when isEqualItems(selection, selectedItem) =>
            None
          | (_, selectedItem) =>
            switch (selectedItem) {
            | Placeholder => None
            | selectedItem => Some(selectedItem)
            }
          };

        onChange->Option.map(onChange => onChange(newSelection))->ignore;

        setSelection(_ => newSelection);
      },
      [|selection|],
    );

  let isSelected =
    React.useCallback1(
      item => {selection->Option.mapWithDefault(false, isEqualItems(item))},
      [|selection|],
    );

  {selection, selectItem, isSelected};
};

type multipleSelection = {
  selection: array(item),
  selectItem: item => unit,
  isSelected: item => bool,
};

let getMultipleSelectionByValue = (items, values) => {
  values->Array.map(value => {
    items->Array.getBy(isEqualItems(value))->Option.getExn
  });
};

let useMultipleSelection =
    (
      ~value: option(array(item))=?,
      ~defaultValue: option(array(item))=?,
      ~onChange: option(array(item) => unit)=?,
      ~items: array(item),
    )
    : multipleSelection => {
  let isControlled = value->Option.isSome;

  let (selection, setSelection) =
    React.useState(_ =>
      defaultValue->Option.mapWithDefault(
        [||],
        getMultipleSelectionByValue(items),
      )
    );

  let selection =
    !isControlled
      ? selection
      : value->Option.mapWithDefault(
          [||],
          getMultipleSelectionByValue(items),
        );

  let setSelection = !isControlled ? setSelection : (fn => fn([||])->ignore);

  let selectItem =
    React.useCallback1(
      selectedItem => {
        let newSelection =
          switch (
            selection->Array.getIndexBy(selection =>
              isEqualItems(selection, selectedItem)
            )
          ) {
          | Some(_) =>
            Js.Array.filter(
              item => !isEqualItems(item, selectedItem),
              selection,
            )
          | None =>
            switch (selectedItem) {
            | Placeholder => [||]
            | selectedItem => Js.Array.concat([|selectedItem|], selection)
            }
          };

        onChange->Option.map(fn => fn(newSelection))->ignore;

        setSelection(_ => newSelection);
      },
      [|selection|],
    );

  let isSelected =
    React.useCallback1(
      item => {
        selection
        ->Array.getBy(selection => isEqualItems(selection, item))
        ->Option.isSome
      },
      [|selection|],
    );

  {selection, selectItem, isSelected};
};

type listboxInputContext = {
  api: BsDownshift.Select.t(item),
  isSelected: item => bool,
};

let listboxInputContext: React.Context.t(option(listboxInputContext)) =
  React.createContext(None);

let useListboxInputContext = () =>
  React.useContext(listboxInputContext)->Option.getExn;

module ListboxInputProvider = {
  let makeProps = (~value, ~children, ()) => {
    "value": Some(value),
    "children": children,
  };
  let make = React.Context.provider(listboxInputContext);
};

module ListboxInputPopover = {
  [@react.component]
  let make =
    React.forwardRef((~className="", ~children: React.element, ref_) => {
      let {api} = useListboxInputContext();

      <Listbox.Popover
        ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
        isOpen={api.isOpen}
        className>
        children
      </Listbox.Popover>;
    });
};

module ListboxInputList = {
  [@react.component]
  let make =
    React.forwardRef(
      (
        ~className="",
        ~onBlur: option(ReactEvent.Focus.t => unit)=?,
        ~children: React.element,
        ref_,
      ) => {
      let {api} = useListboxInputContext();

      let menuProps =
        api.getMenuProps(
          Select.menuPropsOptions(
            ~onBlur?,
            ~ref=
              ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef),
            (),
          ),
        );

      <Listbox.List
        ref={ReactDOMRe.Ref.callbackDomRef(menuProps.ref)}
        isOpen={api.isOpen}
        ariaLabelledby={menuProps.ariaLabelledby}
        ariaActivedescendant={menuProps.ariaActivedescendant}
        role={menuProps.role}
        id={menuProps.id}
        onMouseLeave={menuProps.onMouseLeave}
        onKeyDown={menuProps.onKeyDown}
        onBlur={menuProps.onBlur}
        tabIndex={menuProps.tabIndex}
        className={Cn.make([className, "max-w-sm"])}>
        {api.isOpen ? children : React.null}
      </Listbox.List>;
    });
};

module ListboxInputButton = {
  [@react.component]
  let make =
    React.forwardRef(
      (
        ~size: option(ButtonInput.size)=?,
        ~id: option(string)=?,
        ~onClick: option(ReactEvent.Mouse.t => unit)=?,
        ~disabled: option(bool)=?,
        ~autoFocus: option(bool)=?,
        ~isInvalid: option(bool)=?,
        ~className="",
        ~children: React.element,
        ref_,
      ) => {
      let {api} = useListboxInputContext();

      let toggleButtonProps =
        api.getToggleButtonProps(
          Select.toggleButtonPropsOptions(
            ~id?,
            ~onClick?,
            ~disabled?,
            ~ref=
              ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef),
            (),
          ),
        );

      <ButtonInput
        ref={ReactDOMRe.Ref.callbackDomRef(toggleButtonProps.ref)}
        disabled={toggleButtonProps.disabled}
        ?isInvalid
        ?autoFocus
        ?size
        ariaLabel="toggle menu"
        ariaLabelledby={toggleButtonProps.ariaLabelledby}
        ariaExpanded={toggleButtonProps.ariaExpanded}
        id={toggleButtonProps.id}
        onClick={toggleButtonProps.onClick}
        className>
        children
      </ButtonInput>;
    });
};

module ListboxInputOption = {
  [@react.component]
  let make =
    React.forwardRef(
      (
        ~item: item,
        ~index: int,
        ~onClick: option(ReactEvent.Mouse.t => unit)=?,
        ~disabled: option(bool)=?,
        ~className="",
        ~children: React.element,
        ref_,
      ) => {
      let {api, isSelected} = useListboxInputContext();

      let itemProps =
        api.getItemProps(
          Select.itemPropsOptions(~item, ~index, ~disabled?, ~onClick?, ()),
        );

      <Listbox.Option
        ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
        isHighlighted={api.highlightedIndex === index}
        id={itemProps.id}
        disabled={itemProps.disabled}
        ariaSelected={itemProps.ariaSelected === "true"}
        role={itemProps.role}
        onMouseMove={itemProps.onMouseMove}
        onClick={itemProps.onClick}
        className={Cn.make([
          className,
          "pr-8",
          "italic"->Cn.ifTrue(item === Placeholder),
        ])}>
        {isSelected(item)
           ? <BsReactIcons.MdCheck className="mr-4 w-6 h-6 flex-none" />
           : <span className="mr-4 w-6 h-6 flex-none" />}
        <span className="truncate"> children </span>
      </Listbox.Option>;
    });
};

module ListboxInputGroupLabel = Toolkit__Ui_Listbox.ListboxGroupLabel;

module ListboxInputLabel = Toolkit__Ui_Listbox.ListboxLabel;

module ListboxInputDivider = Toolkit__Ui_Listbox.ListboxDivider;

module ListboxInputSelectProvider = {
  [@react.component]
  let make =
      (
        ~id: option(string)=?,
        ~name: option(string)=?,
        ~value: option(item)=?,
        ~defaultValue: option(item)=?,
        ~items: array(item),
        ~itemToString: item => string,
        ~onChange: option(option(item) => unit)=?,
        ~required: option(bool)=?,
        ~children: (option(item), array(item)) => React.element,
      ) => {
    let {selection, selectItem, isSelected}: selection =
      useSelection(~items, ~value?, ~defaultValue?, ~onChange?);

    let stateReducer =
        (
          _state: Select.state(item),
          {action, changes}: Select.actionAndChanges(item),
        ) => {
      switch (action->Select.action) {
      | MenuKeyDownEnter
      | MenuKeyDownSpaceButton
      | ItemClick
      | FunctionSelectItem =>
        let selectedItem =
          changes.selectedItem->Js.Nullable.toOption->Option.getExn;
        selectItem(selectedItem);
        changes;

      | _ => changes
      };
    };

    let api =
      Select.(
        useSelect(
          selectOptions(
            ~items,
            ~stateReducer,
            ~itemToString=
              item =>
                item
                ->Js.Nullable.toOption
                ->Option.mapWithDefault("", itemToString),
            ~selectedItem=selection->Js.Nullable.fromOption,
            (),
          ),
        )
      );

    <ListboxInputProvider value={api, isSelected}>
      <div className="relative">
        <input
          type_="hidden"
          ?name
          ?id
          ?required
          readOnly=true
          value={selection->Option.mapWithDefault("", itemToString)}
        />
        {children(selection, items)}
      </div>
    </ListboxInputProvider>;
  };
};

module ListboxInputSelect = {
  [@react.component]
  let make =
      (
        ~id: option(string)=?,
        ~name: option(string)=?,
        ~placeholder="Select",
        ~value: option('value)=?,
        ~defaultValue: option('value)=?,
        ~items: array('value),
        ~itemToLabel: option('value => string)=?,
        ~itemToId: option('value => string)=?,
        ~onChange: option(option('value) => unit)=?,
        ~onBlur: option(ReactEvent.Focus.t => unit)=?,
        ~required: option(bool)=?,
        ~disabled=false,
        ~isInvalid: option(bool)=?,
        ~autoFocus: option(bool)=?,
        ~size=`md,
        ~hasPlaceholderOption=false,
        ~className="",
      ) => {
    let itemToId = value =>
      itemToId->Option.mapWithDefault(value->Obj.magic, itemToId =>
        value->itemToId
      );

    let itemToString = item =>
      item
      ->Item.mapItem(((_, value)) => {
          itemToLabel->Option.mapWithDefault(value->Obj.magic, itemToLabel =>
            value->itemToLabel
          )
        })
      ->Option.getWithDefault("");

    let items =
      items
      ->Array.map(value => Item.item(value->itemToId, value->Item.toValue))
      ->Array.concat(hasPlaceholderOption ? [|Placeholder|] : [||], _);

    let value =
      value->Option.flatMap(value =>
        items->Array.getBy(item =>
          item
          ->Item.mapItem(((key, _)) => {key === value->itemToId})
          ->Option.getWithDefault(false)
        )
      );

    let defaultValue =
      defaultValue->Option.flatMap(value =>
        items->Array.getBy(item =>
          item
          ->Item.mapItem(((key, _)) => {key === value->itemToId})
          ->Option.getWithDefault(false)
        )
      );

    let onChange =
      onChange->Option.map((onChange, value) =>
        onChange(
          value->Option.flatMap(Item.mapItem(_, ((_, value)) => value)),
        )
      );

    <ListboxInputSelectProvider
      ?onChange ?name ?value ?defaultValue ?required itemToString items>
      {(selection, items) => {
         <>
           <ListboxInputButton
             ?autoFocus
             disabled
             ?isInvalid
             ?id
             size
             className={Cn.make([className, "w-full flex justify-between"])}>
             <span className="truncate font-normal">
               {{
                  value
                  ->Option.mapWithDefault(selection, value =>
                      Some(getSelectionByValue(items, value))
                    )
                  ->Option.mapWithDefault(placeholder, item =>
                      item->itemToString
                    );
                }
                ->React.string}
             </span>
             <BsReactIcons.MdArrowDropDown
               className="ml-2 w-6 h-6 -my-1 flex-none"
             />
           </ListboxInputButton>
           <ListboxInputPopover className="-mt-12">
             <ListboxInputList ?onBlur>
               {items
                ->Array.mapWithIndex((index, item) => {
                    switch (item) {
                    | Placeholder =>
                      <ListboxInputOption key="placeholder" item index>
                        placeholder->React.string
                      </ListboxInputOption>
                    | Item(key, _) =>
                      <ListboxInputOption key item index>
                        {item->itemToString->React.string}
                      </ListboxInputOption>
                    }
                  })
                ->React.array}
             </ListboxInputList>
           </ListboxInputPopover>
         </>;
       }}
    </ListboxInputSelectProvider>;
  };
};

module ListboxInputMultiSelectProvider = {
  [@react.component]
  let make =
      (
        ~id: option(string)=?,
        ~name: option(string)=?,
        ~value: option(array(item))=?,
        ~defaultValue: option(array(item))=?,
        ~itemToString: item => string,
        ~items: array(item),
        ~onChange: option(array(item) => unit)=?,
        ~required: option(bool)=?,
        ~children: (array(item), array(item)) => React.element,
      ) => {
    let {selection, selectItem, isSelected}: multipleSelection =
      useMultipleSelection(~items, ~value?, ~defaultValue?, ~onChange?);

    let stateReducer =
        (
          state: Select.state(item),
          {action, changes}: Select.actionAndChanges(item),
        ) => {
      switch (action->Select.action) {
      | MenuKeyDownEnter
      | MenuKeyDownSpaceButton
      | ItemClick
      | FunctionSelectItem =>
        let selectedItem =
          changes.selectedItem->Js.Nullable.toOption->Option.getExn;
        selectItem(selectedItem);
        {
          ...changes,
          isOpen:
            switch (selectedItem) {
            | Placeholder => false
            | Item(_) => true
            },
          highlightedIndex: state.highlightedIndex,
        };

      | _ => changes
      };
    };

    let api =
      Select.(
        useSelect(
          selectOptions(
            ~items,
            ~stateReducer,
            ~itemToString=
              item =>
                item
                ->Js.Nullable.toOption
                ->Option.mapWithDefault("", itemToString),
            ~selectedItem=selection->Array.get(0)->Js.Nullable.fromOption,
            (),
          ),
        )
      );

    <ListboxInputProvider value={api, isSelected}>
      <div className="relative">
        <input
          type_="hidden"
          ?name
          ?id
          ?required
          readOnly=true
          value={selection->Obj.magic}
        />
        {children(selection, items)}
      </div>
    </ListboxInputProvider>;
  };
};

module ListboxInputMultiSelect = {
  [@react.component]
  let make =
      (
        ~id: option(string)=?,
        ~name: option(string)=?,
        ~placeholder="Select",
        ~value: option(array('value))=?,
        ~defaultValue: option(array('value))=?,
        ~items: array('value),
        ~itemToLabel: option('value => string)=?,
        ~itemToId: option('value => string)=?,
        ~onChange: option(array('value) => unit)=?,
        ~onBlur: option(ReactEvent.Focus.t => unit)=?,
        ~required: option(bool)=?,
        ~disabled=false,
        ~isInvalid: option(bool)=?,
        ~autoFocus: option(bool)=?,
        ~size=`md,
        ~hasPlaceholderOption=false,
        ~className="",
      ) => {
    let itemToId = value =>
      itemToId->Option.mapWithDefault(value->Obj.magic, itemToId =>
        value->itemToId
      );

    let itemToString = item =>
      item
      ->Item.mapItem(((_, value)) => {
          itemToLabel->Option.mapWithDefault(value->Obj.magic, itemToLabel =>
            value->itemToLabel
          )
        })
      ->Option.getWithDefault("");

    let items =
      items
      ->Array.map(value => Item.item(value->itemToId, value->Item.toValue))
      ->Array.concat(hasPlaceholderOption ? [|Placeholder|] : [||], _);

    let value =
      value->Option.map(value =>
        value->Array.keepMap(value =>
          items->Array.getBy(item =>
            item
            ->Item.mapItem(((key, _)) => {key === value->itemToId})
            ->Option.getWithDefault(false)
          )
        )
      );

    let defaultValue =
      defaultValue->Option.map(value =>
        value->Array.keepMap(value =>
          items->Array.getBy(item =>
            item
            ->Item.mapItem(((key, _)) => {key === value->itemToId})
            ->Option.getWithDefault(false)
          )
        )
      );

    let onChange =
      onChange->Option.map((onChange, value) =>
        onChange(
          value->Array.keepMap(Item.mapItem(_, ((_, value)) => value)),
        )
      );

    <ListboxInputMultiSelectProvider
      ?onChange ?name ?value ?defaultValue ?required items itemToString>
      {(selection, items) => {
         <>
           <ListboxInputButton
             ?autoFocus
             disabled
             ?isInvalid
             ?id
             size
             className={Cn.make([className, "w-full flex justify-between"])}>
             <span className="truncate font-normal">
               {switch (
                  value->Option.mapWithDefault(selection, value =>
                    getMultipleSelectionByValue(items, value)
                  )
                ) {
                | [||] => placeholder
                | items =>
                  items->Array.map(itemToString)->Js.Array.joinWith(", ", _)
                }}
               ->React.string
             </span>
             <BsReactIcons.MdArrowDropDown
               className="ml-2 w-6 h-6 -my-1 flex-none"
             />
           </ListboxInputButton>
           <ListboxInputPopover className="-mt-12">
             <ListboxInputList ?onBlur>
               {items
                ->Array.mapWithIndex((index, item) => {
                    switch (item) {
                    | Placeholder =>
                      <ListboxInputOption key="placeholder" item index>
                        placeholder->React.string
                      </ListboxInputOption>
                    | Item(key, _) =>
                      <ListboxInputOption key item index>
                        {item->itemToString->React.string}
                      </ListboxInputOption>
                    }
                  })
                ->React.array}
             </ListboxInputList>
           </ListboxInputPopover>
         </>;
       }}
    </ListboxInputMultiSelectProvider>;
  };
};

module Popover = ListboxInputPopover;
module Provider = ListboxInputProvider;
module List = ListboxInputList;
module Button = ListboxInputButton;
module Option = ListboxInputOption;
module Label = ListboxInputLabel;
module GroupLabel = ListboxInputGroupLabel;
module Divider = ListboxInputDivider;
module SelectProvider = ListboxInputSelectProvider;
module Select = ListboxInputSelect;
module MultiSelectProvider = ListboxInputMultiSelectProvider;
module MultiSelect = ListboxInputMultiSelect;
