type size = [ | `xs | `sm | `md | `lg | `custom(int)];
type color = [ | `primary | `success | `danger | `neutral];
type style = [ | `default | `colored(color)];

[@react.component]
let make:
  (
    ~isVisible: bool,
    ~title: React.element=?,
    ~body: React.element,
    ~hide: unit => unit,
    ~size: size=?, // default: `md
    ~type_: style=?, // default: `default
    ~footer: React.element=?
  ) =>
  React.element;
