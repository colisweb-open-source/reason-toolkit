module Header = {
  type action = {
    label: React.element,
    event: unit => unit,
  };

  type size = [ | `sm | `md];
  [@react.component]
  let make =
      (
        ~className="",
        ~children=React.null,
        ~action: option(action)=?,
        ~actionDisabled: option(bool)=?,
      ) => {
    <div className="flex flex-row items-center mb-4">
      <div
        className={Cn.make([
          className,
          "font-light font-display text-gray-800 flex justify-between items-center w-full border-b border-primary-500",
        ])}>
        children
      </div>
      {action->Option.mapWithDefault(React.null, ({label, event}) => {
         <Toolkit__Ui_Button
           variant=`pill
           size=`xs
           color=`info
           onClick={_ => event()}
           disabled={actionDisabled->Option.getWithDefault(false)}
           className="flex-shrink-0 ml-4">
           label
         </Toolkit__Ui_Button>
       })}
    </div>;
  };
};

module Body = {
  [@react.component]
  let make = (~className="", ~children=React.null) => {
    <div className={Cn.make([className, "h-full"])}> children </div>;
  };
};

module FixedBody = {
  [@react.component]
  let make = (~className="", ~children=React.null) => {
    <Body>
      <div className={Cn.make([className, "overflow-y-auto h-full"])}>
        children
      </div>
    </Body>;
  };
};

module Message = {
  type variant = [ | `info | `success | `warning | `error];

  [@react.component]
  let make = (~variant, ~className="", ~children=React.null) => {
    let icon = {
      switch (variant) {
      | `info => <BsReactIcons.MdInfo className="w-6 h-6 mr-2 text-info-600" />
      | `success =>
        <BsReactIcons.MdCheckCircle
          className="w-6 h-6 mr-2 text-success-600"
        />
      | `warning =>
        <BsReactIcons.MdWarning className="w-6 h-6 mr-2 text-warning-600" />
      | `error =>
        <BsReactIcons.MdError className="w-6 h-6 mr-2 text-danger-600" />
      };
    };

    let color =
      switch (variant) {
      | `info => "text-info-600 bg-info-50"
      | `success => "text-success-600 bg-success-50"
      | `warning => "text-warning-600 bg-warning-50"
      | `error => "text-danger-600 bg-danger-50"
      };

    <div className={Cn.make([className, "px-6 py-4 flex", color])}>
      icon
      <div> children </div>
    </div>;
  };
};

module Footer = {
  [@react.component]
  let make = (~className="", ~children=React.null) => {
    <div className={Cn.make([className, "flex items-center"])}>
      children
    </div>;
  };
};

[@react.component]
let make = (~className="", ~children=React.null, ~customMinHeight=200) => {
  <div
    className={Cn.make([
      className,
      "rounded-lg shadow-sm bg-white p-6",
      Css.(style([minHeight(customMinHeight->px)])),
    ])}>
    children
  </div>;
};
