type status = [ | `success | `error | `warning | `info];

[@react.component]
let make = (~title, ~description=?, ~status, ~className=?) => {
  <div
    className={Cn.make([
      "py-2 px-4 flex items-center",
      switch (status) {
      | `success => "bg-success-50"
      | `error => "bg-danger-50"
      | `warning => "bg-warning-50"
      | `info => "bg-info-50"
      },
      className->Option.getWithDefault(""),
    ])}>
    {switch (status) {
     | `success =>
       <BsReactIcons.MdCheckCircle size=28 className="text-success-500" />
     | `error => <BsReactIcons.MdWarning size=28 className="text-danger-600" />
     | `warning =>
       <BsReactIcons.MdWarning size=28 className="text-warning-500" />
     | `info =>
       <BsReactIcons.MdInfoOutline size=28 className="text-info-500" />
     }}
    <div className="mx-3">
      <div className={description->Option.isSome ? "font-bold" : ""}>
        title
      </div>
      {description->Option.getWithDefault(React.null)}
    </div>
  </div>;
};
