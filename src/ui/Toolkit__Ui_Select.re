[@react.component]
let make =
    (
      ~options,
      ~onChange,
      ~placeholder=?,
      ~defaultValue=?,
      ~isDisabled=?,
      ~isInvalid=?,
      ~className=?,
      ~id=?,
      ~value=?,
    ) => {
  <div className="relative">
    <select
      ?id
      disabled={isDisabled->Option.getWithDefault(false)}
      ariaDisabled={isDisabled->Option.getWithDefault(false)}
      className={Cn.make([
        "border rounded transition-all duration-150 ease-in-out py-2 pl-4 pr-8 appearance-none w-full bg-white text-gray-800 leading-tight focus:outline-none focus:shadow-none focus:border-info-500 disabled:bg-gray-200 disabled:text-gray-700 focus:z30 relative",
        switch (isInvalid) {
        | Some(true) => "border-danger-500 shadow-danger-500"
        | _ => "border-gray-300"
        },
        className->Cn.unpack,
      ])}
      ?defaultValue
      ?value
      onChange={event => {
        let value = ReactEvent.Form.target(event)##value;

        onChange(value);
      }}>
      {placeholder->Option.mapWithDefault(React.null, placeholder => {
         <option> placeholder->React.string </option>
       })}
      {options
       ->Array.mapWithIndex((i, (label, value, enabled)) => {
           <option
             key={"select_option_" ++ i->Int.toString}
             value
             disabled={!enabled}>
             label
           </option>
         })
       ->React.array}
    </select>
    <div
      className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-800">
      <BsReactIcons.MdArrowDropDown className="w-6 h-6 z-30" />
    </div>
  </div>;
};
