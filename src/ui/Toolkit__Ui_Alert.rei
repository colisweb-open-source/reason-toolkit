type status = [ | `success | `error | `warning | `info];

[@react.component]
let make:
  (
    ~title: React.element,
    ~description: React.element=?,
    ~status: status,
    ~className: string=?
  ) =>
  React.element;
