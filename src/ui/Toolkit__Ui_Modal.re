type size = [ | `xs | `sm | `md | `lg | `custom(int)];
type color = [ | `primary | `success | `danger | `neutral];
type style = [ | `default | `colored(color)];

let sizeRule = size => {
  let value =
    switch (size) {
    | `xs => 480
    | `sm => 600
    | `md => 768
    | `lg => 900
    | `custom(value) => value
    };
  //
  Css.(style([width(pct(100.)), important(maxWidth(px(value)))]));
};

let modalStyle = (~type_) => {
  switch (type_) {
  | `default => "rounded"
  | `colored(color) =>
    switch (color) {
    | `primary => "rounded-lg border-2 border-primary-500"
    | `success => "rounded-lg border-2 border-success-500"
    | `danger => "rounded-lg border-4 border-danger-700"
    | `neutral => "rounded-lg"
    }
  };
};

let headerStyle = (~type_) => {
  switch (type_) {
  | `default => ""
  | `colored(color) =>
    switch (color) {
    | `primary => "bg-primary-500 rounded-t"
    | `success => "bg-success-500 rounded-t"
    | `danger => "bg-danger-700"
    | `neutral => "bg-neutral-700 rounded-t-lg"
    }
  };
};

let titleStyle = (~type_) => {
  switch (type_) {
  | `default => "w-full border-b border-info-500"
  | `colored(_color) => "text-white"
  };
};

let closeIconStyle = (~type_) => {
  switch (type_) {
  | `default => "hover:bg-gray-200"
  | `colored(_) => "hover:bg-white hover:text-black text-white"
  };
};

let blockBody = () => Css.(global("body", [overflow(`hidden)]));
let resetBody = () => Css.(global("body", [overflow(`auto)]));
Css.(
  global(
    "[data-reach-dialog-overlay]",
    [important(background(rgba(74, 115, 120, `num(0.5))))],
  )
);
Css.(
  global(
    "[data-reach-dialog-content]",
    [important(background(transparent))],
  )
);

[@react.component]
let make =
    (
      ~isVisible,
      ~title: option(React.element)=?,
      ~body: React.element,
      ~hide,
      ~size=`md,
      ~type_=`default,
      ~footer=?,
    ) => {
  <ReachUi.Dialog isOpen=isVisible onDismiss=hide className={sizeRule(size)}>
    <div
      className={Cn.make([
        "bg-white pb-5 z-40 shadow-lg w-full",
        modalStyle(~type_),
      ])}>
      <header
        className={Cn.make([
          "flex items-center justify-between mb-4 pl-5 pr-3 pt-2 pb-1",
          headerStyle(~type_),
        ])}>
        {title->Option.mapWithDefault(React.null, title => {
           <h2
             className={Cn.make([
               "text-2xl pb-1 font-display",
               titleStyle(~type_),
             ])}>
             title
           </h2>
         })}
        <button
          onClick={_ => hide()}
          className={Cn.make([
            title->Option.isSome ? "ml-4" : "ml-auto",
            "p-1 rounded-full",
            closeIconStyle(~type_),
          ])}>
          <BsReactIcons.MdClose size=28 />
        </button>
      </header>
      <div className="px-1 sm:px-2 md:px-5"> body </div>
      {footer->Option.mapWithDefault(React.null, footer =>
         <div className="flex flex-row justify-end mt-6 pr-4"> footer </div>
       )}
    </div>
  </ReachUi.Dialog>;
};
