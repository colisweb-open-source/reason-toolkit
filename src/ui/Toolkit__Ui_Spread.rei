[@react.component]
let make: (~props: 'a, ~children: React.element) => React.element;
