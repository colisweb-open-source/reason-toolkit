module Make = (StateLenses: Reform.Config) => {
  module Form = {
    include Reform.Make(StateLenses);

    let use =
        (
          ~initialState,
          ~schema=Validation.Schema([||]),
          ~onSubmit=?,
          ~onSubmitFail=ignore,
          ~i18n=ReSchemaI18n.default,
          ~validationStrategy=OnDemand,
          (),
        )
        : api =>
      use(
        ~initialState,
        ~schema,
        ~onSubmit=
          form => {
            onSubmit
            ->Option.map(onSubmit => {
                onSubmit(form)
                ->Promise.tap(
                    fun
                    | Ok(_) => form.send(SetFormState(SubmitSucceed))
                    | Error(error) =>
                      form.send(SetFormState(SubmitFailed(Some(error)))),
                  )
              })
            ->ignore;
            None;
          },
        ~onSubmitFail,
        ~i18n,
        ~validationStrategy,
        (),
      );
  };

  module Wrapper = {
    [@react.component]
    let make = (~id=?, ~className=?, ~children) => {
      let form = Form.useFormContext()->Option.getExn;
      <form
        ?id
        ?className
        onSubmit={event => {
          ReactEvent.Synthetic.preventDefault(event);
          form.submit();
        }}>
        children
        <ReachUi.VisuallyHidden>
          <input type_="submit" hidden=true />
        </ReachUi.VisuallyHidden>
      </form>;
    };
  };
};
