// See https://github.com/streamich/react-use/blob/master/src

// ----------------------
// Utils
// ----------------------

module SafeIncrement = {
  let minInt = (-999999999);
  let maxInt = 1000000000;

  let increment = (num: int): int => {
    num !== maxInt ? num + 1 : minInt;
  };
};

let useIsFirstMount = ReactUse.useFirstMountState;
let useUpdateEffect = ReactUse.useUpdateEffect;
let useIsMounted = ReactUse.useMountedState;
let useTimeoutFn = ReactUse.useTimeoutFn;
let useDebounce = ReactUse.useDebounce;
let useDebounce2 = ReactUse.useDebounce2;
let useDebounce3 = ReactUse.useDebounce3;
let useDebounce4 = ReactUse.useDebounce4;

let useUpdate = () => {
  let (_, setState) = React.useState(_ => 0);

  React.useCallback1(() => setState(SafeIncrement.increment), [||]);
};

let useGetSet = ReactUse.useGetSet;
let usePrevious = ReactUse.usePrevious;
let useInitialPrevious = value => {
  let previousRef = React.useRef(value);

  React.useEffect(() => {
    previousRef.current = value;
    None;
  });

  previousRef.current;
};
let usePreviousDistinct = ReactUse.usePreviousDistinct;

let useInitialPreviousDistinct = (~compare=(a, b) => a === b, value) => {
  let previousRef = React.useRef(value);
  let currentRef = React.useRef(value);
  let isFirstMount = useIsFirstMount();

  if (!isFirstMount && !compare(currentRef.current, value)) {
    previousRef.current = currentRef.current;
    currentRef.current = value;
  };

  previousRef.current;
};

// ----------------------
// useFetcher
// ----------------------

type fetcherOptions = BsSwr.fetcherOptions;

let fetcherOptions = BsSwr.fetcherOptions;

type fetcher('data) = (
  'data,
  bool,
  unit => Promise.t(result(bool, Js.Promise.error)),
);

let updateFetcherData = BsSwr.mutate;

let triggerFetcher = BsSwr.trigger;

let useFetcher =
    (~options: option(fetcherOptions)=?, key: 'key, fn: 'fn): fetcher('data) => {
  let fetcher =
    BsSwr.useSwr(
      key,
      fn,
      Js.Obj.assign(
        switch (options) {
        | Some(options) => options->Obj.magic
        | None => Js.Obj.empty()
        },
        {"suspense": true},
      ),
    );

  (
    fetcher.data->Obj.magic,
    fetcher.isValidating,
    () =>
      fetcher.revalidate(BsSwr.revalidateOptions())
      ->Promise.Js.fromBsPromise
      ->Promise.Js.toResult,
  );
};

let useOptionalFetcher =
    (~options: option(fetcherOptions)=?, key: 'key, fn: 'fn): fetcher('data) => {
  let fetcher =
    BsSwr.useSwrOptional(
      key,
      fn,
      Js.Obj.assign(
        switch (options) {
        | Some(options) => options->Obj.magic
        | None => Js.Obj.empty()
        },
        {"suspense": true},
      ),
    );

  (
    fetcher.data->Obj.magic,
    fetcher.isValidating,
    () =>
      fetcher.revalidate(BsSwr.revalidateOptions())
      ->Promise.Js.fromBsPromise
      ->Promise.Js.toResult,
  );
};

// ----------------------
// useDisclosure
// ----------------------

type disclosure = {
  isOpen: bool,
  show: unit => unit,
  hide: unit => unit,
  toggle: unit => unit,
};

let useDisclosure = (~defaultIsOpen=?, ()) => {
  let (isOpen, setIsOpen) =
    React.useState(() => defaultIsOpen->Belt.Option.getWithDefault(false));

  {
    isOpen,
    show: React.useCallback(() => setIsOpen(_ => true)),
    hide: React.useCallback(() => setIsOpen(_ => false)),
    toggle: React.useCallback(() => setIsOpen(isOpen => !isOpen)),
  };
};

// ----------------------
// useClipboard
// This hook require the presence of a Toolkit Ui Snackbar provider for clipboard notifications to work
// ----------------------

type clipboard = {
  value: string,
  copy: unit => unit,
  hasCopied: bool,
};

let useClipboard =
    (~onCopyNotificationMessage: option(string)=?, value: string) => {
  let (hasCopied, setHasCopied) = React.useState(() => false);
  let onCopy =
    React.useCallback1(
      () => {
        onCopyNotificationMessage->Option.forEach(message =>
          Toolkit__Ui_Snackbar.show(~title=message, ~variant=`success, ())
        );

        let didCopy = BsCopyToClipboard.copy(value);
        setHasCopied(_ => didCopy);
      },
      [|value|],
    );

  React.useEffect1(
    () => {
      hasCopied
        ? {
          let id =
            Js.Global.setTimeout(() => {setHasCopied(_ => false)}, 1500);
          Some(() => Js.Global.clearTimeout(id));
        }
        : None
    },
    [|hasCopied|],
  );

  {value, copy: onCopy, hasCopied};
};

// ----------------------
// useRequest
// ----------------------

external promiseErrorToJsObj: Js.Promise.error => Js.Exn.t = "%identity";

type requestState('data, 'error) =
  | NotAsked
  | Loading
  | Done(result('data, 'error));

let useRequest = (~debounce=true, fn, deps) => {
  let lastCallId = React.useRef(0);
  let canceled = React.useRef(false);
  let (state, set) = React.useState(() => NotAsked);
  let isMounted = useIsMounted(.);

  let trigger =
    React.useCallback1(
      args => {
        lastCallId.current = lastCallId.current->SafeIncrement.increment;
        let callId = lastCallId.current;

        set(_ => Loading);

        canceled.current = false;

        fn(args)
        ->Promise.map(result => {
            let isCanceled =
              (debounce ? callId !== lastCallId.current : false)
              || canceled.current;

            if (isMounted(.) && !isCanceled) {
              set(_ => Done(result));
            };

            (result, isCanceled);
          });
      },
      deps,
    );

  let cancel = React.useCallback0(() => {canceled.current = true});

  (state, trigger, cancel);
};
