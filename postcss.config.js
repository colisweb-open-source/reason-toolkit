const path = require("path");
const glob = require("glob");
const matchAll = require("string.prototype.matchall");

const tailwindConfig = path.join(__dirname, "src/tailwind/tailwind.config.js");

module.exports = {
  plugins: [
    require("tailwindcss")(tailwindConfig),
    ...(process.env.NODE_ENV === "production" && !process.env.STORYBOOK
      ? [require("autoprefixer")]
      : []),
    require("postcss-preset-env")({
      autoprefixer: {
        flexbox: "no-2009",
      },
      stage: 3,
    }),
  ],
};
