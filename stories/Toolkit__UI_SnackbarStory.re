open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Snackbar.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/Snackbar", ());

let success = () => {
  <div>
    <Snackbar.Provider />
    <div className="mb-4 flex flex-row flex-col-gap-4">
      <span>
        <Button
          onClick={_ =>
            Snackbar.show(~title="Success", ~variant=`success, ())
          }>
          "Success"->React.string
        </Button>
      </span>
      <span>
        <Button
          onClick={_ =>
            Snackbar.show(
              ~title="Success",
              ~description="Description"->React.string,
              ~variant=`success,
              (),
            )
          }>
          "Success with description"->React.string
        </Button>
      </span>
    </div>
  </div>;
};

success->CSF.addMeta(
  ~name="success",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
    <Snackbar.Provider />
    <div className="mb-4 flex flex-row flex-col-gap-4">
      <span>
        <Button
          onClick={_ =>
            Snackbar.show(~title="Success", ~variant=`success, ())
          }>
          "Success"->React.string
        </Button>
      </span>
      <span>
        <Button
          onClick={_ =>
            Snackbar.show(
              ~title="Success",
              ~description="Description"->React.string,
              ~variant=`success,
              (),
            )
          }>
          "Success with description"->React.string
        </Button>
      </span>
    </div>
  </div>;
          |j}
          />
          <Docs.Story id="components-snackbar--success" />
        </>,
    },
  },
  (),
);

let warning = () => {
  <div>
    <Snackbar.Provider />
    <div className="mb-4 flex flex-row flex-col-gap-4">
      <span>
        <Button
          onClick={_ =>
            Snackbar.show(~title="warning", ~variant=`warning, ())
          }>
          "warning"->React.string
        </Button>
      </span>
      <span>
        <Button
          onClick={_ =>
            Snackbar.show(
              ~title="warning",
              ~description="Description"->React.string,
              ~variant=`warning,
              (),
            )
          }>
          "warning with description"->React.string
        </Button>
      </span>
    </div>
  </div>;
};

warning->CSF.addMeta(
  ~name="warning",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Snackbar.Provider />
  <div className="mb-4 flex flex-row flex-col-gap-4">
    <span>
      <Button
        onClick={_ =>
          Snackbar.show(~title="warning", ~variant=`warning, ())
        }>
        "warning"->React.string
      </Button>
    </span>
    <span>
      <Button
        onClick={_ =>
          Snackbar.show(
            ~title="warning",
            ~description="Description"->React.string,
            ~variant=`warning,
            (),
          )
        }>
        "warning with description"->React.string
      </Button>
    </span>
  </div>
</div>;
          |j}
          />
          <Docs.Story id="components-snackbar--warning" />
        </>,
    },
  },
  (),
);

let danger = () => {
  <div>
    <Snackbar.Provider />
    <div className="mb-4 flex flex-row flex-col-gap-4">
      <span>
        <Button
          onClick={_ => Snackbar.show(~title="danger", ~variant=`danger, ())}>
          "danger"->React.string
        </Button>
      </span>
      <span>
        <Button
          onClick={_ =>
            Snackbar.show(
              ~title="danger",
              ~description="Description"->React.string,
              ~variant=`danger,
              (),
            )
          }>
          "danger with description"->React.string
        </Button>
      </span>
    </div>
  </div>;
};

danger->CSF.addMeta(
  ~name="danger",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Snackbar.Provider />
  <div className="mb-4 flex flex-row flex-col-gap-4">
    <span>
      <Button
        onClick={_ => Snackbar.show(~title="danger", ~variant=`danger, ())}>
        "danger"->React.string
      </Button>
    </span>
    <span>
      <Button
        onClick={_ =>
          Snackbar.show(
            ~title="danger",
            ~description="Description"->React.string,
            ~variant=`danger,
            (),
          )
        }>
        "danger with description"->React.string
      </Button>
    </span>
  </div>
</div>;
          |j}
          />
          <Docs.Story id="components-snackbar--danger" />
        </>,
    },
  },
  (),
);
