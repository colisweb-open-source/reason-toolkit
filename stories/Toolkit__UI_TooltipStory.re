open BsStorybook;
open Storybook;
open Toolkit__Ui;

let default =
  CSF.make(
    ~title="Components/Tooltip",
    ~parameters={
      "docs": {
        "page": () =>
          <>
            <Docs.Title />
            <Docs.Source
              language="reason"
              code={j|
            <span>
    <Tooltip canBeShowed=true label={<span> "Label"->React.string </span>}>
      <div> "Hover me"->React.string </div>
    </Tooltip>
  </span>;
          |j}
            />
            <span>
              <Tooltip
                canBeShowed=true label={<span> "Label"->React.string </span>}>
                <div> "Hover me"->React.string </div>
              </Tooltip>
            </span>
            <hr className="mt-4" />
            <Docs.Source
              language="reason"
              code={j|
            <span>
    <Tooltip canBeShowed=false label={<span> "Label"->React.string </span>}>
      <div> "Hover me"->React.string </div>
    </Tooltip>
  </span>;
          |j}
            />
            <span>
              <Tooltip
                canBeShowed=false label={<span> "Label"->React.string </span>}>
                <div> "Hover me but nothing"->React.string </div>
              </Tooltip>
            </span>
          </>,
      },
    },
    (),
  );

let example = () => {
  <span>
    <Tooltip
      canBeShowed=true
      label={
        <span>
          "Please consider only if the lift is big enough for the commodity"
          ->React.string
        </span>
      }>
      <div> "Hover me"->React.string </div>
    </Tooltip>
  </span>;
};

example->CSF.addMeta(~name="example", ());
