open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Alert.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/Alert", ());

let success = () =>
  <Alert
    title={"Title"->React.string}
    description={"Description"->React.string}
    status=`success
  />;

success->CSF.addMeta(
  ~name="success",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
            <Alert
    title={"Title"->React.string}
    description={"Description"->React.string}
    status=`success
  />;
          |j}
          />
          <Docs.Story id="components-alert--success" />
        </>,
    },
  },
  (),
);

let error = () =>
  <Alert
    title={"Title"->React.string}
    description={"Description"->React.string}
    status=`error
  />;

error->CSF.addMeta(
  ~name="error",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
            <Alert
    title={"Title"->React.string}
    description={"Description"->React.string}
    status=`error
  />;
          |j}
          />
          <Docs.Story id="components-alert--error" />
        </>,
    },
  },
  (),
);

let warning = () =>
  <Alert
    title={"Title"->React.string}
    description={"Description"->React.string}
    status=`warning
  />;

warning->CSF.addMeta(
  ~name="warning",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
            <Alert
  title={"Title"->React.string}
  description={"Description"->React.string}
  status=`warning
/>;
          |j}
          />
          <Docs.Story id="components-alert--warning" />
        </>,
    },
  },
  (),
);

let info = () =>
  <Alert
    title={"Title"->React.string}
    description={"Description"->React.string}
    status=`info
  />;

info->CSF.addMeta(
  ~name="info",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
            <Alert
  title={"Title"->React.string}
  description={"Description"->React.string}
  status=`info
/>;
          |j}
          />
          <Docs.Story id="components-alert--info" />
        </>,
    },
  },
  (),
);
