open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Tag.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/Tag", ());

let palette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("primary", `primary),
      ("info", `info),
      ("danger", `danger),
      ("warning", `warning),
      ("success", `success),
      ("neutral", `neutral),
    |]);

  let size =
    Js.Dict.fromArray([|
      ("xs", `xs),
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
    |]);
  <div className="flex flex-col flex-gap-2 flex-1">
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (key, color)) => {
         <div
           key={i->Int.toString ++ key->Obj.magic}
           className="flex items-center flex-col-gap-2">
           {size
            ->Js.Dict.entries
            ->Array.mapWithIndex((j, (key, size)) => {
                <div key={(i + j)->Int.toString ++ key->Obj.magic}>
                  <Tag color size> "content"->React.string </Tag>
                </div>
              })
            ->React.array}
         </div>
       })
     ->React.array}
  </div>;
};

palette->CSF.addMeta(
  ~name="palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<Tag color size> "content"->React.string </Tag>
          |j}
          />
          <Docs.Story id="components-tag--palette" />
        </>,
    },
  },
  (),
);

let plain = () => {
  <Tag color=`gray size=`md> "content"->React.string </Tag>;
};

plain->CSF.addMeta(
  ~name="plain",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<Tag color=`gray size=`md> "content"->React.string </Tag>;
          |j}
          />
          <Docs.Story id="components-tag--plain" />
        </>,
    },
  },
  (),
);

let outline = () => {
  <Tag color=`gray variant=`outline size=`md> "content"->React.string </Tag>;
};

outline->CSF.addMeta(
  ~name="outline",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<Tag color=`gray variant=`outline size=`md> "content"->React.string </Tag>;
          |j}
          />
          <Docs.Story id="components-tag--outline" />
        </>,
    },
  },
  (),
);
