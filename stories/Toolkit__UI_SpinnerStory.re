open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Spinner.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/Spinner", ());

let defaultPalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("current", `current),
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("success", `success),
      ("primary", `primary),
      ("warning", `warning),
      ("info", `info),
      ("danger", `danger),
    |]);

  let sizes =
    Js.Dict.fromArray([|
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
      ("xl", `xl),
    |]);

  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40">
             colorText->React.string
           </strong>
           <div className="flex items-center flex-col-gap-6 mb-2">
             {sizes
              ->Js.Dict.entries
              ->Array.mapWithIndex((k, (sizeText, size)) => {
                  <div key={(i + k)->Int.toString ++ colorText ++ sizeText}>
                    <Spinner color size />
                  </div>
                })
              ->React.array}
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

defaultPalette->CSF.addMeta(
  ~name="default palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reason"
            code={j|
            <Spinner color=`primary size=`md />
          |j}
          />
          <Spinner color=`primary size=`md />
        </>,
    },
  },
  (),
);
