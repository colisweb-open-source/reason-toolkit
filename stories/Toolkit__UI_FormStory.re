open BsStorybook.Story;
open Storybook;
open Toolkit__Ui;

let _module = [%bs.raw "module"];

storiesOf("Form/TextareaInput", _module)
->addDecorator(Knobs.withKnobs)
->add("default", () => {
    <div>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        "Label"->React.string
      </Label>
      <TextareaInput id="test" placeholder="Test" />
    </div>
  })
->add("with error", () => {
    <div>
      <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
        "Label"->React.string
      </Label>
      <TextareaInput id="test" placeholder="Test" isInvalid=true />
    </div>
  })
->ignore;

module DayPickerExample = {
  open BsReactDayPicker;

  [@react.component]
  let make = () => {
    let (activityPeriodStart, setActivityPeriodStart) =
      React.useState(() => None);
    let (activityPeriodEnd, setActivityPeriodEnd) =
      React.useState(() => None);

    let range =
      ReactDayPicker.{
        from: activityPeriodStart->Js.Nullable.fromOption,
        to_: activityPeriodEnd->Js.Nullable.fromOption,
      };

    let modifiers =
      React.useMemo1(
        () => ReactDayPicker.{start: range.from, end_: range.to_},
        [|range.from, range.to_|],
      );

    <ReactDayPicker.DayPicker
      modifiers
      className="input-date-range"
      showOutsideDays=true
      numberOfMonths=2
      firstDayOfWeek=1
      selectedDays=(activityPeriodStart->Obj.magic, range)
      onDayClick={from => {
        let range =
          ReactDayPicker.DateUtils.addDayToRange(
            from,
            {
              from: activityPeriodStart->Js.Nullable.fromOption,
              to_: activityPeriodEnd->Js.Nullable.fromOption,
            },
          );
        setActivityPeriodStart(_ => range.from->Js.Nullable.toOption);
        setActivityPeriodEnd(_ => range.to_->Js.Nullable.toOption);
      }}
    />;
  };
};

storiesOf("Form/react-day-picker", _module)
->addDecorator(Knobs.withKnobs)
->add("DayPicker", () => {<DayPickerExample />})
->add("DayPickerInput", () => {
    <div>
      <BsReactDayPicker.ReactDayPicker.DayPickerInput
        showOutsideDays=true
        firstDayOfWeek=1
      />
    </div>
  })
->ignore;
