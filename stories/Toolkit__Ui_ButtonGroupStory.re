open BsStorybook;
open Storybook;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_ButtonGroup.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/ButtonGroup", ());

let sizePalette = () => {
  let sizes = Js.Dict.fromArray([|("md", `md), ("lg", `xl)|]);

  let (selectedOption, setSelectedOption) = React.useState(() => `option1);

  <div>
    {sizes
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (sizeText, size)) => {
         <div
           key={i->Int.toString ++ sizeText}
           className="flex flex-row mb-4 items-center">
           <strong className="w-40"> sizeText->React.string </strong>
           <Toolkit__Ui_ButtonGroup className="my-4 self-center" size>
             <button
               className={"selected"->Cn.ifTrue(selectedOption === `option1)}
               onClick={_ => setSelectedOption(_ => `option1)}>
               "Option 1"->React.string
             </button>
             <button
               className={"selected"->Cn.ifTrue(selectedOption === `option2)}
               onClick={_ => setSelectedOption(_ => `option2)}>
               "Option 2"->React.string
             </button>
             <button
               className={"selected"->Cn.ifTrue(selectedOption === `option3)}
               onClick={_ => setSelectedOption(_ => `option3)}>
               "Option 3"->React.string
             </button>
           </Toolkit__Ui_ButtonGroup>
         </div>
       })
     ->React.array}
  </div>;
};

sizePalette->CSF.addMeta(
  ~name="size palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reason"
            code={j|
            let (selectedOption, setSelectedOption) = React.useState(() => `option1);

            <Toolkit__Ui_ButtonGroup
             className="my-4 self-center" size>
             <button
               className={"selected"->Cn.ifTrue(selectedOption === `option1)}
               onClick={_ => setSelectedOption(_ => `option1)}>
               "Option 1"->React.string
             </button>
             <button
               className={"selected"->Cn.ifTrue(selectedOption === `option2)}
               onClick={_ => setSelectedOption(_ => `option2)}>
               "Option 2"->React.string
             </button>
             <button
               className={"selected"->Cn.ifTrue(selectedOption === `option3)}
               onClick={_ => setSelectedOption(_ => `option3)}>
               "Option 3"->React.string
             </button>
           </Toolkit__Ui_ButtonGroup>
          |j}
          />
        </>,
    },
  },
  (),
);
