open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Checkbox.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Form/Checkbox", ());

let sample = () =>
  <Checkbox name="test" value="test"> "children"->React.string </Checkbox>;

sample->CSF.addMeta(
  ~name="sample",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<Checkbox name="test" value="test"> "children"->React.string </Checkbox>
          |j}
          />
          <Docs.Story id="form-checkbox--sample" />
        </>,
    },
  },
  (),
);

let checked = () =>
  <Checkbox checked=true name="test" value="test">
    "children"->React.string
  </Checkbox>;

checked->CSF.addMeta(
  ~name="checked",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<Checkbox name="test" checked=true value="test"> "children"->React.string </Checkbox>
          |j}
          />
          <Docs.Story id="form-checkbox--checked" />
        </>,
    },
  },
  (),
);

let sizes = () =>
  <>
    <Checkbox
      checked=true
      name="test"
      value="test"
      size=`xs
      className="mb-4">
      "xs"->React.string
    </Checkbox>
    <Checkbox
      checked=true
      name="test"
      value="test"
      size=`sm
      className="mb-4">
      "sm"->React.string
    </Checkbox>
    <Checkbox
      checked=true
      name="test"
      value="test"
      size=`md
      className="mb-4">
      "md"->React.string
    </Checkbox>
    <Checkbox
      checked=true
      name="test"
      value="test"
      size=`lg
      className="mb-4">
      "lg"->React.string
    </Checkbox>
  </>;

sizes->CSF.addMeta(
  ~name="sizes",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<>
  <Checkbox
    checked=true
    name="test"
    value="test"
    size=`xs
    className="mb-4">
    "xs"->React.string
  </Checkbox>
  <Checkbox
    checked=true
    name="test"
    value="test"
    size=`sm
    className="mb-4">
    "sm"->React.string
  </Checkbox>
  <Checkbox
    checked=true
    name="test"
    value="test"
    size=`md
    className="mb-4">
    "md"->React.string
  </Checkbox>
  <Checkbox
    checked=true
    name="test"
    value="test"
    size=`lg
    className="mb-4">
    "lg"->React.string
  </Checkbox>
</>
          |j}
          />
          <Docs.Story id="form-checkbox--sizes" />
        </>,
    },
  },
  (),
);
