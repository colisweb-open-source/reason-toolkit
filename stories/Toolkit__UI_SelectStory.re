open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Select.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Form/Select", ());

let native = () =>
  <div>
    <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
      "Label"->React.string
    </Label>
    <Select
      onChange={_ => ()}
      id="test"
      placeholder="This is a placeholder"
      options=[|
        ("label"->React.string, "value", true),
        ("label2"->React.string, "value2", true),
        ("label disabled"->React.string, "value3", false),
      |]
    />
  </div>;

native->CSF.addMeta(
  ~name="native",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
    "Label"->React.string
  </Label>
  <Select
    onChange={_ => ()}
    id="test"
    placeholder="This is a placeholder"
    options=[|
      ("label"->React.string, "value", true),
      ("label2"->React.string, "value2", true),
      ("label disabled"->React.string, "value3", false),
    |]
  />
</div>
          |j}
          />
          <Docs.Story id="form-select--native" />
        </>,
    },
  },
  (),
);

let reactSelect = () => {
  open BsReactSelect;
  let options = [|
    {label: "Label", value: "value"},
    {label: "Label2", value: "value2"},
    {label: "Label3", value: "value3"},
  |];

  let isDisabled = Knobs.boolean("isDisabled", false);
  let isInvalid = Knobs.boolean("isInvalid", false);

  <div>
    <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
      "Label"->React.string
    </Label>
    <ReactSelect
      name="test"
      isDisabled
      classNamePrefix="react-select"
      className={Cn.make(["errored"->Cn.ifTrue(isInvalid)])}
      placeholder="Placeholder"
      onChange={v => Js.log(v)}
      options
    />
    <div className="h-10" />
    <Label htmlFor="test2" optionalMessage={"Optional"->React.string}>
      "Searchable select with multiple value"->React.string
    </Label>
    <ReactSelectMultiple
      name="test2"
      isDisabled
      defaultValue=[||]
      classNamePrefix="react-select"
      className={Cn.make(["errored"->Cn.ifTrue(isInvalid)])}
      placeholder="Placeholder"
      onChange={v => Js.log(v)}
      options
    />
  </div>;
};

reactSelect->CSF.addMeta(
  ~name="react-select",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Source
            language="reasonml"
            code={j|
open BsReactSelect;
let options = [|
  {label: "Label", value: "value"},
  {label: "Label2", value: "value2"},
  {label: "Label3", value: "value3"},
|];

let isDisabled = Knobs.boolean("isDisabled", false);
let isInvalid = Knobs.boolean("isInvalid", false);

<div>
  <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
    "Label"->React.string
  </Label>
  <ReactSelect
    name="test"
    isDisabled
    classNamePrefix="react-select"
    className={Cn.make(["errored"->Cn.ifTrue(isInvalid)])}
    placeholder="Placeholder"
    onChange={v => Js.log(v)}
    options
  />
  <div className="h-10" />
  <Label htmlFor="test2" optionalMessage={"Optional"->React.string}>
    "Searchable select with multiple value"->React.string
  </Label>
  <ReactSelectMultiple
    name="test2"
    isDisabled
    defaultValue=[||]
    classNamePrefix="react-select"
    className={Cn.make(["errored"->Cn.ifTrue(isInvalid)])}
    placeholder="Placeholder"
    onChange={v => Js.log(v)}
    options
  />
</div>;
          |j}
          />
          <Docs.Story id="form-select--react-select" />
        </>,
    },
  },
  (),
);
