type export = {title: string};

module Knobs = {
  [@bs.module "@storybook/addon-knobs"]
  external withKnobs: BsStorybook.Main.decorator = "withKnobs";

  [@bs.module "@storybook/addon-knobs"]
  external boolean: (string, bool) => bool = "boolean";

  [@bs.module "@storybook/addon-knobs"]
  external select: (string, Js.Dict.t('a), 'a) => 'a = "select";

  [@bs.module "@storybook/addon-knobs"]
  external array: (string, array('a), 'a) => string = "array";

  [@bs.module "@storybook/addon-knobs"]
  external text: (string, string) => string = "text";

  [@bs.module "@storybook/addon-knobs"]
  external int: (string, int) => int = "number";

  [@bs.module "@storybook/addon-knobs"]
  external float: (string, float) => string = "number";

  type range('v) = {
    range: bool,
    min: 'v,
    max: 'v,
    step: int,
  };

  [@bs.module "@storybook/addon-knobs"]
  external intRange: (string, int, range(int)) => string = "number";

  [@bs.module "@storybook/addon-knobs"]
  external floatRange: (string, float, range(float)) => string = "number";

  [@bs.module "@storybook/addon-knobs"]
  external date: (string, Js.Date.t) => string = "date";
};

module Docs = {
  module Title = {
    [@bs.module "@storybook/addon-docs/blocks"] [@react.component]
    external make: unit => React.element = "Title";
  };
  module Subtitle = {
    [@bs.module "@storybook/addon-docs/blocks"] [@react.component]
    external make: unit => React.element = "Subtitle";
  };
  module Description = {
    [@bs.module "@storybook/addon-docs/blocks"] [@react.component]
    external make: unit => React.element = "Description";
  };
  module Story = {
    [@bs.module "@storybook/addon-docs/blocks"] [@react.component]
    external make: (~id: string) => React.element = "Story";
  };
  module Source = {
    [@bs.module "@storybook/addon-docs/blocks"] [@react.component]
    external make: (~language: string, ~code: string) => React.element =
      "Source";
  };
};

module CodeBlock = {
  [@bs.module "../../../.storybook/CodeBlock.js"] [@react.component]
  external make: (~children: React.element) => React.element = "default";
};
