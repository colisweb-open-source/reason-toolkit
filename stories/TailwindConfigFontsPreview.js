import tailwindConfig from "../src/tailwind/tailwind.config";

const fontFamily = tailwindConfig.theme.fontFamily;
const fontSize = tailwindConfig.theme.fontSize;

export default () => (
  <div>
    {Object.entries(fontFamily).map(([fontValue, fontName]) => {
      return (
        <div className="mb-6 border-b" key={fontValue}>
          <h3 className="text-xl font-bold mb-4">
            <span className="px-2 py-1 bg-gray-100 border rounded">
              {fontValue}
            </span>{" "}
            : {fontName.join(", ")}
          </h3>
          {Object.entries(fontSize).map(([key, fontSize]) => {
            return (
              <div key={key + "bug"}>
                <p
                  style={{
                    fontFamily: fontName.join(", "),
                    fontSize,
                  }}
                >
                  {key}
                </p>
              </div>
            );
          })}
        </div>
      );
    })}
  </div>
);
