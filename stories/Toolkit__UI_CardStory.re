open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Card.rei"]
external source: 'a = "default";

let default =
  CSF.make(
    ~title="Components/Card",
    ~parameters={
      "docs": {
        "page": () =>
          <>
            <CodeBlock> source </CodeBlock>
            <Docs.Source
              language="reasonml"
              code={j|
            <Card>
  <Card.Header> "title"->React.string </Card.Header>
  <Card.Body> body </Card.Body>
</Card>;
          |j}
            />
            <Docs.Story id="components-card--default" />
            <hr />
            <h2> "Custom"->React.string </h2>
            <Docs.Source
              language="reasonml"
              code={j|
              <Card>
  <Card.Header size=`sm>
    "title"->React.string
    <Button> "Action"->React.string </Button>
  </Card.Header>
  <Card.Body> body </Card.Body>
</Card>;
          |j}
            />
            <Docs.Story id="components-card--custom" />
          </>,
      },
    },
    (),
  );

let defaultPalette = () => {
  let body =
    {j|
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu pretium leo. Phasellus imperdiet, tellus sit amet egestas varius,
        nisl est viverra lorem, in porttitor est diam id quam. Phasellus dapibus, est in malesuada tristique, eros ipsum interdum sapien,
        vitae auctor sapien magna sit amet justo. Nunc commodo sapien non tincidunt posuere. Nulla maximus porta tellus, vitae pulvinar
        velit cursus a. Nam egestas eget est nec vestibulum. Mauris porta urna eget ultrices consectetur. Phasellus ultricies erat nec
        tellus dapibus pulvinar. Nullam at odio ultrices, feugiat dolor a, vulputate ex. In hac habitasse platea dictumst. Maecenas ut sagittis eros.
        Cras sit amet urna maximus, congue nulla a, rutrum leo. Maecenas semper aliquet sapien convallis eleifend.
      |j}
    ->React.string;

  <Card>
    <Card.Header> "title"->React.string </Card.Header>
    <Card.Body> body </Card.Body>
  </Card>;
};

defaultPalette->CSF.addMeta(~name="default", ());

let custom = () => {
  let body =
    {j|
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu pretium leo. Phasellus imperdiet, tellus sit amet egestas varius,
        nisl est viverra lorem, in porttitor est diam id quam. Phasellus dapibus, est in malesuada tristique, eros ipsum interdum sapien,
        vitae auctor sapien magna sit amet justo. Nunc commodo sapien non tincidunt posuere. Nulla maximus porta tellus, vitae pulvinar
        velit cursus a. Nam egestas eget est nec vestibulum. Mauris porta urna eget ultrices consectetur. Phasellus ultricies erat nec
        tellus dapibus pulvinar. Nullam at odio ultrices, feugiat dolor a, vulputate ex. In hac habitasse platea dictumst. Maecenas ut sagittis eros.
        Cras sit amet urna maximus, congue nulla a, rutrum leo. Maecenas semper aliquet sapien convallis eleifend.
      |j}
    ->React.string;

  <Card>
    <Card.Header>
      "title"->React.string
      <Button> "Action"->React.string </Button>
    </Card.Header>
    <Card.FixedBody> body </Card.FixedBody>
  </Card>;
};

custom->CSF.addMeta(~name="custom", ());

let withAction = () => {
  let body =
    {j|
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu pretium leo. Phasellus imperdiet, tellus sit amet egestas varius,
        nisl est viverra lorem, in porttitor est diam id quam. Phasellus dapibus, est in malesuada tristique, eros ipsum interdum sapien,
        vitae auctor sapien magna sit amet justo. Nunc commodo sapien non tincidunt posuere. Nulla maximus porta tellus, vitae pulvinar
        velit cursus a. Nam egestas eget est nec vestibulum. Mauris porta urna eget ultrices consectetur. Phasellus ultricies erat nec
        tellus dapibus pulvinar. Nullam at odio ultrices, feugiat dolor a, vulputate ex. In hac habitasse platea dictumst. Maecenas ut sagittis eros.
        Cras sit amet urna maximus, congue nulla a, rutrum leo. Maecenas semper aliquet sapien convallis eleifend.
      |j}
    ->React.string;
  <>
    <Card>
      <Card.Header
        action={
          label: "Click me"->React.string,
          event: () => Snackbar.show(~title="Hello", ~variant=`success, ()),
        }>
        "title"->React.string
      </Card.Header>
      <Card.Body> body </Card.Body>
    </Card>
    <Snackbar.Provider />
  </>;
};

withAction->CSF.addMeta(
  ~name="withAction",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<>
  <Card>
    <Card.Header
      size=`sm
      action={
        label: "Click me"->React.string,
        event: () => Snackbar.show(~title="Hello", ~variant=`success, ()),
      }>
      "title"->React.string
    </Card.Header>
    <Card.Body> body </Card.Body>
  </Card>
  <Snackbar.Provider />
</>;
          |j}
          />
          <Docs.Story id="components-card--with-action" />
        </>,
    },
  },
  (),
);
